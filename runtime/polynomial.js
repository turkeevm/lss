const isEmpty = require('lodash/isEmpty');
const keyBy = require('lodash/keyBy');
const forEach = require('lodash/forEach');
const mapValues = require('lodash/mapValues');
const map = require('lodash/map');
const memoize = require('lodash/memoize');
const {typeInfo, warnOnce} = require('./util');

//const x = $unit.make(1, 'px');
//const y = $unit.make(1, 'px');

//const z = x.mul(y);

const units = [
//  [suffix,    domain,        addGroup addFactor],
    ['em',      'length',       'f1',   1],
    ['ex',      'length',       'f2',   1],
    ['ch',      'length',       'f3',   1],
    ['rem',     'length',       'f4',   1],

    ['vh',      'length',       'v1',   1],
    ['vw',      'length',       'v2',   1],
    ['vmin',    'length',       'v3',   1],
    ['vmax',    'length',       'v4',   1],

    ['px',      'length',       'px',   4800],
    ['q',       'length',       'px',   5080],
    ['mm',      'length',       'px',   1270],
    ['cm',      'length',       'px',   127],
    ['in',      'length',       'px',   50],
    ['pt',      'length',       'px',   3600],
    ['pc',      'length',       'px',   300],

    ['%',       'length',       '%',    1],

    ['deg',     'angle',        'tu',   360],
    ['grad',    'angle',        'tu',   400],
    ['rad',     'angle',        'tu',   2*Math.PI],
    ['turn',    'angle',        'tu',   1],

    ['Hz',      'freq',         'fq',   1000],
    ['kHz',     'freq',         'fq',   1],

    ['dpi',     'resolution',   'd',    50],
    ['dpcm',    'resolution',   'd',    127],
    ['dppx',    'resolution',   'd',    4800],

    ['s',       'time',         't',    1],
    ['ms',      'time',         't',    1000]
];

const knownUnits = keyBy(units, 0);
const suffix2domain = mapValues(knownUnits, 1);
const suffix2addSig = mapValues(knownUnits, 2);
const suffix2factor = mapValues(knownUnits, 3);

const addFactor = memoize(
    sig => sig.keys.reduce((acc, suffix) => acc * Math.pow(suffix2factor[suffix], sig.desc[suffix]), 1),
    sig => sig.report
);

class Sig {
    constructor(desc, _keys = null) {
        this.desc = desc;
        this._keys = _keys;
        this._report = null;
    }
    get keys() {
        if (this._keys === null) {
            this._keys = Object.keys(this.desc).sort();
        }
        return this._keys;
    }
    get report() {
        if (this._report === null) {
            const subReport = (keys, inv = false) => {
                return keys.map((unitName) => {
                    const power = inv
                        ? - this.desc[unitName]
                        : this.desc[unitName];
                    if (power === 1) {
                        return unitName;
                    }
                    return unitName + '^' + power;
                }).join('*');
            };

            const posReport = subReport(this.keys.filter(k => this.desc[k] > 0)) || 1;
            const negReport = subReport(this.keys.filter(k => this.desc[k] < 0), true);

            this._report = negReport
                ? `${posReport}/${negReport}`
                : `${posReport}`;
        }
        return this._report;
    }
    get inverted() {
        return new Sig(mapValues(this.desc, x => -x), this._keys);
    }
    get isTrivial() {
        return this.keys.length === 1 && this.desc[this.keys[0]] === 1;
    }
    convert(mapping) {
        const acc = {};
        forEach(this.desc, (power, suffix) => {
            const newKey = mapping[suffix];
            acc[newKey] = (acc[newKey] || 0) + power;
        });
        return new Sig(acc);
    }
}

const nativeCompares = {
    eq: (a, b) =>  a === b,
    neq: (a, b) =>  a !== b,
    gt: (a, b) =>  a > b,
    gte: (a, b) =>  a >= b,
    lt: (a, b) =>  a < b,
    lte: (a, b) =>  a <= b,
};

class Monomial {
    constructor(value, signature, _domainSig = null, _addSig = null) {
        this.value = value;
        this.signature = signature;
        this._domainSig = _domainSig;
        this._addSig = _addSig;
    }

    get domainSig() {
        if (this._domainSig === null) {
            this._domainSig = this.signature.convert(suffix2domain);
        }
        return this._domainSig;
    }

    get addSig() {
        if (this._addSig === null) {
            this._addSig = this.signature.convert(suffix2addSig);
        }
        return this._addSig;
    }

    get inverted() {
        return new Monomial(1 / this.value, this.signature.inverted);
    }

    get report() {
        return this.value + this.signature.report;
    }

    typeInfo() {
        return `Monomial(${this.signature.report})`;
    }

    inc() {
        return new Monomial(this.value + 1, this.signature, this._domainSig, this._addSig);
    }

    dec() {
        return new Monomial(this.value - 1, this.signature, this._domainSig, this._addSig);
    }

    mul(that) {
        if (typeof that === 'number') {
            return new Monomial(this.value * that, this.signature);
        }
        if (that instanceof Monomial) {
            const newSigDesc = Object.assign({}, this.signature.desc);
            that.signature.keys.forEach(key => {
                newSigDesc[key] = (newSigDesc[key] || 0) + that.signature.desc[key];
                if (newSigDesc[key] === 0) {
                    delete newSigDesc[key];
                }
            });

            return new Monomial(
                this.value * that.value,
                new Sig(newSigDesc)
            ).reduceSignature();
        }
        throw new Error(`cannot multiply ${typeInfo(this)} with ${typeInfo(that)}`);
    }

    reduceSignature() {
        let reduceFactor = 1;
        let reduced = false;
        const plainSig = Object.assign({}, this.signature.desc);
        const pos = this.signature.keys.filter(k => plainSig[k] > 0);
        const neg = this.signature.keys.filter(k => plainSig[k] < 0);

        pos.forEach(posUnit => {
            for (let i = 0; i < neg.length; i++) {
                const negUnit = neg[i];
                if (suffix2addSig[posUnit] === suffix2addSig[negUnit]) {
                    reduced = true;
                    const posPower = plainSig[posUnit];
                    const negPower = plainSig[negUnit];
                    const delta = Math.min(posPower, -negPower);
                    reduceFactor *=
                        Math.pow(suffix2factor[posUnit], -posPower) *
                        Math.pow(suffix2factor[negUnit], -negPower);
                    plainSig[posUnit] -= delta;
                    plainSig[negUnit] += delta;

                    if (plainSig[negUnit] === 0) {
                        delete plainSig[negUnit];
                    }
                    if (plainSig[posUnit] === 0) {
                        delete plainSig[posUnit];
                        return;
                    }
                }
            }
        });

        if (isEmpty(plainSig)) {
            return this.value * reduceFactor;
        }
        if (!reduced) {
            return this;
        }
        return new Monomial(
            this.value * reduceFactor,
            new Sig(plainSig)
        );
    }

    div(that) {
        if (typeof that === 'number') {
            return this.mul(1/that);
        }
        return this.mul(that.inverted);
    }

    negate() {
        return new Monomial(-this.value, this.signature, this._domainSig, this._addSig);
    }

    add(that) {
        if (!(that instanceof Monomial)) {
            throw new Error(`cannot apply 'add' to ${typeInfo(this)} and ${typeInfo(that)}`);
        }
        if (this.addSig.report !== that.addSig.report) {
            const err = `units ${this.signature.report} and ${that.signature.report} are not compatible for addition`;
            if (this.domainSig.report !== that.domainSig.report) {
                throw new Error(err);
            }
            throw new Error(`${err}. You probably meant to use ++ instead of +`);
        }

        const thisFactor = addFactor(this.signature);
        const thatFactor = addFactor(that.signature);

        return new Monomial((this.value * thisFactor + that.value * thatFactor) / thisFactor, this.signature)
    }

    sub(that) {
        if (!(that instanceof Monomial)) {
            throw new Error(`cannot apply 'sub' to ${typeInfo(this)} and ${typeInfo(that)}`);
        }
        return this.add(that.negate());
    }

    addPoly(that) {
        if (that instanceof Polynomial) {
            return new Polynomial([this]).addPoly(that);
        }
        if (that instanceof Monomial) {
            return new Polynomial([this]).addPoly(new Polynomial([that]));
        }
    }

    _compare(op, that) {
        if (that instanceof Polynomial) {
            return new Polynomial([this])._compare(op, that);
        }
        if (!(that instanceof Monomial)) {
            throw new Error(`cannot compare ${typeInfo(this)} and ${typeInfo(that)}`);
        }
        const thisFactor = addFactor(this.signature);
        const thatFactor = addFactor(that.signature);

        return nativeCompares[op](this.value * thisFactor, that.value * thatFactor);
    }

    gt(that) {
        return this._compare('gt', that);
    }
    lt(that) {
        return this._compare('lt', that);
    }
    gte(that) {
        return this._compare('gte', that);
    }
    lte(that) {
        return this._compare('lte', that);
    }
    eq(that) {
        return this._compare('eq', that);
    }
    neq(that) {
        return this._compare('neq', that);
    }
    toString() {
        //законвертить по addSig
        return this.report;
    }
}

class Polynomial {
    constructor(monomials, _byAddSigReport = null) {
        this.monomials = monomials;
        this._byAddSigReport = _byAddSigReport;
    }

    get byAddSigReport() {
        if (this._byAddSigReport === null) {
            this._byAddSigReport = keyBy(this.monomials, m => m.addSig.report);
        }
        return this._byAddSigReport;
    }

    get domainSigReport() {
        return this.monomials[0].domainSig.report;
    }

    _addLike(op, that) {
        if (!(that instanceof Polynomial)) {
            throw new Error(`don't know how to apply ${op} to ${typeInfo(this)} and ${typeInfo(that)}`);
        }

        if (this.domainSigReport !== that.domainSigReport) {
            throw new Error(`cannot apply ${op} to values from different domains (${this.domainSigReport} and ${that.domainSigReport})`);
        }

        const newMonomials = that.monomials.map(thatMonomial => this.byAddSigReport[thatMonomial.addSig.report]
            ? this.byAddSigReport[thatMonomial.addSig.report][op](thatMonomial)
            : thatMonomial
        );

        this.monomials.forEach(monomial => {
            if (!that.byAddSigReport[monomial.addSig.report]) {
                newMonomials.push(monomial);
            }
        });

        return new Polynomial(newMonomials);
    }

    addPoly(that) {
        if (that instanceof Monomial) {
            return this._addLike('addPoly', new Polynomial([that]));
        }
        return this._addLike('addPoly', that);
    }
    subPoly(that) {
        if (that === 0) {
            return this;
        }
        if (!(that instanceof Polynomial)) {
            throw new Error(`don't know how to subtract ${typeInfo(that)} from ${typeInfo(this)}`);
        }
        return this.addPoly(that.negate());
    }
    negate() {
        return new Polynomial(this.monomials.map(monomial => monomial.negate()));
    }
    mul(that) {
        if (typeof that === 'number') {
            return new Polynomial(this.monomials.map(monomial => monomial.mul(that)));
        }
        if (that instanceof Polynomial) {
            const partials = that.monomials.map(thatMonomial => new Polynomial(this.monomials.map(thisMonomial => thisMonomial.mul(thatMonomial))));
            return partials.reduce((acc, partial) => acc.addPoly(partial));
        }
        throw new Error(`cannot multiply ${typeInfo(this)} with ${typeInfo(that)}`);
    }
    div(that) {
        if (typeof that === 'number') {
            return this.mul(1 / that);
        }
        if (that instanceof Polynomial) {
            if (that.monomials.length === 0) {
                return this.mul(Infinity);
            }
            if (that.monomials.length !== 1) {
                throw new Error(`can't divide non-trivial polynomials`);
            }
            return this.mul(new Polynomial([that.monomials[0].inverted]));
        }
        throw new Error(`cannot divide ${typeInfo(this)} by ${typeInfo(that)}`);
    }
    get report() {
        return this.monomials.map(monomial => monomial.report).join('+').replace(/\+-/g, '-');
    }
    _compare(op, that) {
        const poly = this._addLike(op, that);
        const probableAnswer = poly.monomials[0];
        const reachedConsensus = poly.monomials.every(monomial => monomial === probableAnswer);

        if (reachedConsensus) {
            return probableAnswer;
        }
        throw new Error(`${this.report} and ${that.report} are not statically comparable`);
    }
    typeInfo() {
        return `Polynomial(${this.report})`
    }
    gt(that) {
        return this._compare('gt', that);
    }
    lt(that) {
        return this._compare('lt', that);
    }
    gte(that) {
        return this._compare('gte', that);
    }
    lte(that) {
        return this._compare('lte', that);
    }
    eq(that) {
        return this._compare('eq', that);
    }
    neq(that) {
        return this._compare('neq', that);
    }
    toString() {
        if (!this.monomials[0].addSig.isTrivial) {
            throw new Error(`${this.report} is not a valid css value`);
        }
        if (this.monomials.length === 1) return this.monomials[0].toString();
        return `calc(${this.monomials.map(monomial => monomial.toString()).join('+')})`.replace(/\+-/g, '-');
    }
}

const make = (value, unit) => new Monomial(value, new Sig({[unit]: 1}));


module.exports = {make, knownUnits, Polynomial, Monomial};
