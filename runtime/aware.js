const conjMap = (...allArgs) => {
    const args = allArgs.slice(0, -1);
    const f = allArgs[allArgs.length - 1];
    const mapped = array(args);
    const {MediaValue} = require('./media');
    if (mapped instanceof MediaValue) {
        return mapped.map(argsCase => f(...argsCase));
    }
    return null;
};

const atom = (x) => x;
const array = (xs, i = 0) => {
    const {MediaValue} = require('./media');
    for (;i < xs.length; i++) {
        if (xs[i] instanceof MediaValue) {
            return xs[i].map((x) => array([
                ...xs.slice(0, i),
                x,
                ...xs.slice(i + 1)
            ], i + 1));
        }
    }
    return xs;
};
const seq = (...items) => (xs) => array(items.map((item, i) => item(xs[i])));
const arrayOf = (item) => (args) => array(args.map(arg => item(arg)));
const propAssignment = (assignment) => {
    const {CssPropAssignment} = require('./css');
    const {MediaValue} = require('./media');
    if (assignment.name instanceof MediaValue) {
        return assignment.name.map(
            name => propAssignment(new CssPropAssignment(name, assignment.value))
        );
    }
    if (assignment.value instanceof MediaValue) {
        return assignment.value.map(
            value => new CssPropAssignment(assignment.name, value)
        );
    }
    return assignment;
};

const applier = (checker) => (f, args) => {
    const {MediaValue} = require('./media');
    const mArgs = checker(args);
    if (mArgs instanceof MediaValue) {
        return mArgs.map(args => f(...args));
    }
    return f(...args);
};

/**
 * rules = [
 *  [selector, [
 *      PropAssignment,
 *      PropAssignment,
 *      ...
 *  ]],
 *  [selector, ...],
 *  ...
 * ]
 */
const mediaAwareKeyframesRules = arrayOf(
    seq(atom, arrayOf(
        propAssignment
    ))
);

module.exports.mediaAwareKeyframesRules = mediaAwareKeyframesRules;
const wrapper = (checker) => {
    const _applier = applier(checker);
    return (f) => (...args) => _applier(f, args);
};

const aware = wrapper(array);

module.exports = aware;
module.exports.awareApply = (f, args) => aware(f).apply(null, args);
module.exports.conjMap = conjMap;

Object.assign(aware, {atom, array, seq, arrayOf, propAssignment, applier, wrapper});
