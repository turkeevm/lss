const aware = require('./aware');
const Color = require('color');
const CssName = require('./css/primitives/CssName');
const CssColor = require('./css/primitives/CssColor');
const {typeInfo} = require('./util');

const rgb = aware((r, g, b) => CssColor.fromColor(null, Color([r, g, b])));

const ensureColor = (aColor) => {
    const cssColor = aColor instanceof CssColor
        ? aColor
        : aColor instanceof CssName
            ? aColor.asColor()
            : null;

    if (cssColor === null) {
        throw new Error(`color cannot be represented through ${typeInfo(aColor)}`);
    }

    return cssColor;
};

const addAlpha = (model) => aware((...args) => {
    if (args.length >= 4) {
        return CssColor.fromColor(null, Color(args));
    }
    const [aColor, alpha] = args;
    const cssColor = ensureColor(aColor);
    return CssColor.fromColor(null, cssColor.color[model]().alpha(alpha));
});

const hsl = aware((h, s, l) => CssColor.fromColor(null, Color([h, s, l], 'hsl')));

const rgba = addAlpha('rgb');
const hsla = addAlpha('hsl');

const mix = aware((a, b, f) => {
    const cssColorA = ensureColor(a);
    const cssColorB = ensureColor(b);

    return CssColor.fromColor(null, cssColorA.color.mix(cssColorB.color, f));
});


const testRe = (re, str) => new RegExp(re).test(str);

module.exports = {rgb, rgba, hsl, hsla, mix, testRe};
