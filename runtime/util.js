const typeInfo = x => {
    if (typeof x === 'undefined') return 'undefined';
    if (typeof x.typeInfo === 'function') return x.typeInfo();
    if (typeof x.constructor === 'function' && x.constructor.name) return x.constructor.name;
    return typeof x;
};

const warnedWith = {};
const warnOnce = (message) => {
    if (warnedWith[message]) return;
    warnedWith[message] = true;
    console.warn(message);
};

module.exports = {
    typeInfo,
    warnOnce
};
