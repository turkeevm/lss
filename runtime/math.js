const {Monomial, Polynomial} = require('./polynomial');
const {typeInfo} = require('./util');
const aware = require('./aware');

// функции в этом модуле должны гарантировать, только то, что операции над примитивными типами будут
// выполены корректно, а над более сложными будут переданы дальше


const binary = (op, native) => (a, b) => {
    if (typeof a === 'number' && typeof b === 'number') {
        return native(a, b);
    }
    if (typeof a === 'string' && typeof b === 'string') {
        return native(a, b);
    }
    if (a === null || typeof a === 'undefined' || typeof a[op] !== 'function') {
        throw new Error(`operation '${op}' is not defined for types ${typeInfo(a)} and ${typeInfo(b)}`);
    }
    return a[op](b);
};

const addBinary = (name, native) => {
    module.exports[name] = aware(binary(name, native));
};

const addNative = (name, native) => {
    module.exports[name] = aware(native);
};

addBinary('gt', (a, b) => a > b);
addBinary('lt', (a, b) => a < b);
addBinary('lte', (a, b) => a <= b);
addBinary('gte', (a, b) => a >= b);
addBinary('eq', (a, b) => a === b);
addBinary('neq', (a, b) => a !== b);
addBinary('sub', (a, b) => a - b);

addNative('and', (a, b) => a && b);
addNative('or', (a, b) => a || b);
addNative('not', (a) => !a);

const simpleMul = binary('mul', (a, b) => a * b);
const mul = (a, b) => {
    if (typeof a === 'number' && typeof b.mul === 'function') {
        return b.mul(a);
    }
    return simpleMul(a, b);
};
module.exports.mul = aware(mul);

const simpleDiv = binary('div', (a, b) => a / b);
const div = (a, b) => {
    if (typeof a === 'number' && (b instanceof Monomial || b instanceof Polynomial)) {
        return b.inverted.mul(a);
    }
    return simpleDiv(a, b);
};
module.exports.div = aware(div);

module.exports.add = aware((a, b) => {
    const aNative = typeof a === 'number'|| typeof a === 'string';
    const bNative = typeof b === 'number'|| typeof b === 'string';
    if (aNative && bNative) {
        return a + b;
    }

    if (a instanceof Monomial && !(b instanceof Monomial)) {
        throw new Error(`operation 'add' is not defined for types ${typeInfo(a)} and ${typeInfo(b)}`);
    }
    if (typeof a.add !== 'function') {
        throw new Error(`operation 'add' is not defined for ${typeInfo(a)}`);
    }
    return a.add(b)
});

module.exports.addPoly = aware((a, b) => {
    if (a instanceof Monomial || a instanceof Polynomial) return a.addPoly(b);
    if (b instanceof Monomial || b instanceof Polynomial) return b.addPoly(a);
    throw new Error(`operation 'addPoly' is not defined for types ${typeInfo(a)} and ${typeInfo(b)}`);
});

module.exports.negate = aware((x) => {
    if (typeof x === 'number') return -x;
    if (typeof x.negate !== 'function') {
        throw new Error(`operation 'negate' is not defined for ${typeInfo(x)}`);
    }
    return x.negate();
});

module.exports.inc = aware((x) => {
    if (typeof x === 'number') return x + 1;
    if (x instanceof Monomial) return x.inc();
    throw new Error(`cannot increment ${typeInfo(x)}`);
});

module.exports.dec = aware((x) => {
    if (typeof x === 'number') return x - 1;
    if (x instanceof Monomial) return x.dec();
    throw new Error(`cannot decrement ${typeInfo(x)}`);
});

const simpleAccess = aware((obj, key) => {
    const {Range, List} = require('./lists');
    const CssBase = require('./css/frames/CssBase');

    if (obj instanceof Range || obj instanceof List) {
        return obj.lookup(key);
    }
    if (obj instanceof CssBase) {
        return obj.lookup(key);
    }
    return obj[key];
});


module.exports.access = (obj, key) => {
    const {MediaValue} = require('./media');

    if (obj instanceof MediaValue) {
        return obj[key];
    }

    return simpleAccess(obj, key);
};
