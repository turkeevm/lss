const {typeInfo} = require('./util');
const {Monomial, Polynomial, knownUnits, make} = require('./polynomial');
let customUnits = {};

module.exports = {
    resolve: (val, name) => {
        if (knownUnits.hasOwnProperty(name)) return make(val, name);
        if (name in customUnits) {
            const res = customUnits[name](val);
            if (res instanceof Polynomial || res instanceof Monomial || typeof res === 'number') {
                return res;
            }
            throw new Error(`${val}${name} is resolved to ${typeInfo(res)} which is neither unit or plain number`);
        }
        throw new Error(`No such unit ${name}`);
    },
    register: (name, unit) => {
        if (typeof unit !== 'function') {
            throw new Error(`Cannot register unit ${name}. ${typeInfo(unit)} is not a function`)
        }
        if (name in customUnits) {
            throw new Error(`Unit ${name} already exists`);
        }
        customUnits[name] = unit;
    },
    init() {
        customUnits = {};
    }
};
