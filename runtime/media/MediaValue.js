const MediaCase = require('./MediaCase');
const MediaCondition = require('./conditions/MediaCondition');

module.exports = class MediaValue {
    constructor(cases) {
        this.cases = cases;
    }
    forEach(f) {
        this.cases.forEach((mediaCase) => {
            f(mediaCase.value, mediaCase.condition);
        })
    }
    map(f) {
        return new MediaValue(
            this.cases
                .map(mediaCase => {
                    const val = f(mediaCase.value, mediaCase.condition);
                    if (val instanceof MediaValue) {
                        return val.cases.map((childCase) => {
                            return new MediaCase(
                                MediaCondition.conj(mediaCase.condition, childCase.condition),
                                childCase.value
                            );
                        });
                    }
                    return [new MediaCase(mediaCase.condition, val)];
                })
                .reduce((acc, cases) => acc.concat(cases), [])
                .filter(mediaCase => mediaCase.condition !== false)
        );
    }
    toString() {
        console.warn(`MediaValue.toString should never be called directly`);
        return '<css emission error>';
    }
};
