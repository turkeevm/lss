const {warnOnce} = require('../../util');
const MediaCondition = require('./MediaCondition');

module.exports = class BoolMediaCondition extends MediaCondition {
    constructor(name) {
        super();
        this.name = name;
    }
    toString() {
        return this.name;
    }
    covers(that) {
        if (this.name === 'all') return true;
        warnOnce(`this case (${this.name}) is not properly implemented yet`);
        return false;
    }
    contradicts(that) {
        if (that instanceof BoolMediaCondition) {
            warnOnce(`BoolMediaCondition.contradicts: this case (${this.name} x ${that.name}) is not properly implemented yet`);
        }
        return false;
    }
    static isEqual(a, b) {
        return a.name === b.name;
    }
};
