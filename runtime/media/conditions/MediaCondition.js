const {typeInfo} = require('../../util');


module.exports = class MediaCondition {
    conj(that) {
        if (that === false) return false;
        if (that === true || that.covers(this)) return this;
        if (this.covers(that)) return that;
        if (this.contradicts(that)) return false;
        const ConjMediaCondition = require('./ConjMediaCondition');
        return new ConjMediaCondition([this, that]);
    }
    static conj(x, y) {
        if (x === false) return false;
        if (x === true) return y;
        if (y === false) return false;
        if (y === true) return x;
        if (!(x instanceof MediaCondition) || !(y instanceof MediaCondition)) {
            throw new Error(`MediaCondition.combine cannot operate on ${typeInfo(x)} and ${typeInfo(y)}`);
        }
        return x.conj(y);
    }

    static isEqual(a, b) {
        if (typeof a === 'boolean') {
            return a === b;
        }
        if (a === b) {
            return true;
        }
        if (a.constructor !== b.constructor) {
            return false;
        }
        return a.constructor.isEqual(a, b);
    }

    static covers(a, b) {
        if (a === true) return true;
        if (a !== false && b === false) return true;
        if (b === true) return false;
        if (!(a instanceof MediaCondition)) {
            throw new Error(`expected media condition or boolean, got ${typeInfo(a)}`)
        }
        if (!(b instanceof MediaCondition)) {
            throw new Error(`expected media condition or boolean, got ${typeInfo(b)}`);
        }
        return a.covers(b);
    }
};
