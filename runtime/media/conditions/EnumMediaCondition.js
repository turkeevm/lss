const {warnOnce} = require('../../util');
const MediaCondition = require('./MediaCondition');

module.exports = class EnumMediaCondition extends MediaCondition {
    constructor(name, value) {
        super();
        this.name = name;
        this.value = value;
    }
    toString() {
        return `(${this.name}: ${this.value})`;
    }
    covers(that) {
        if (!(that instanceof EnumMediaCondition)) {
            return false;
        }
        return this.name === that.name && this.value === that.value;
    }
    contradicts(that) {
        if (!(that instanceof EnumMediaCondition)) {
            return false;
        }
        if (this.name !== that.name) return false;
        return this.value !== that.value;
    }
    static isEqual(a, b) {
        return a.name === b.name && a.value === b.value;
    }
};
