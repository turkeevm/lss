const MediaCondition = require('./MediaCondition');
const BoolMediaCondition = require('./BoolMediaCondition');
const DimensionalMediaCondition = require('./DimensionalMediaCondition');
const EnumMediaCondition = require('./EnumMediaCondition');

module.exports = class ConjMediaCondition extends MediaCondition {
    constructor(conds) {
        super();
        this.conds = conds;
    }
    toString() {
        return this.conds.join(' and ');
    }
    covers(that) {
        return this.conds.every(cond => cond.covers(that));
    }
    contradicts(that) {
        return this.conds.some(cond => cond.contradicts(that));
    }
    conj(that) {
        let covered = false;
        const conds = this.conds.map(cond => {
            if (cond.covers(that)) {
                covered = true;
                return that;
            }
            return cond;
        });
        return new ConjMediaCondition(covered ? conds : [...conds, that]);
    }
    static isEqual(a, b) {
        return a.covers(b) && b.covers(a);
    }
};