const MediaCondition = require('./MediaCondition');

module.exports = class DimensionalMediaCondition extends MediaCondition {
    constructor(name, op, value) {
        super();
        this.name = name;
        this.op = op;
        this.value = value;
    }
    toString() {
        switch (this.op) {
            case '==': return `(${this.name}: ${this.value})`;
            case '<=': return `(max-${this.name}: ${this.value})`;
            case '>=': return `(min-${this.name}: ${this.value})`;
            default: throw new Error(`DimmensionMediaCondition: don't know how to interpret operator ${op}`);
        }
    }
    covers(that) {
        if (!(that instanceof DimensionalMediaCondition)) {
            return false;
        }
        if (this.name !== that.name) {
            return false;
        }
        const math = require('../../math');
        switch (this.op) {
            case '<=': return that.op !== '>=' && math.gte(this.value, that.value);
            case '>=': return that.op !== '<=' && math.lte(this.value, that.value);
            case '==': return that.op === '==' && math.eq(this.value, that.value);
            default: throw new Error(`Unknown operation ${that.op}`);
        }
    }
    contradicts(that) {
        if (!(that instanceof DimensionalMediaCondition)) {
            return false;
        }
        if (this.name !== that.name) {
            return false;
        }
        if (this.op === that.op) {
            return false;
        }
        const opCase = `${this.op}x${that.op}`;
        const math = require('../../math');
        switch (opCase) {
            case '<=x==': return math.lt(this.value, that.value);
            case '<=x>=': return math.lt(this.value, that.value);
            case '>=x==': return math.gt(this.value, that.value);
            case '>=x<=': return math.gt(this.value, that.value);
            case '==x<=': return math.gt(this.value, that.value);
            case '==x>=': return math.lt(this.value, that.value);
            default: throw new Error(`unknown combination of operators ${this.op} and ${that.op}`);
        }
    }
    static isEqual(a, b) {
        const math = require('../../math');
        return a.name === b.name
            && a.op === b.op
            && math.eq(a.value, b.value);
    }
};
