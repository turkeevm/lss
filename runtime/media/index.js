const MediaValue = require('./MediaValue');
const MediaCase = require('./MediaCase');

const MediaCondition = require('./conditions/MediaCondition');
const DimensionalMediaCondition = require('./conditions/DimensionalMediaCondition');
const BoolMediaCondition = require('./conditions/BoolMediaCondition');
const EnumMediaCondition = require('./conditions/EnumMediaCondition');

module.exports = {
    MediaValue,
    MediaCondition,
    MediaCase,
    make: (cases) => new MediaValue(cases),
    makeCase: (condition, value) => new MediaCase(condition, value),
    conditions: {
        makeDimmension: (name, op, val) => {
            const math = require('../math');
            if (op === '<') {
                val = math.dec(val);
                op = '<=';
            } else if (op === '>') {
                val = math.inc(val);
                op = '>=';
            }
            return new DimensionalMediaCondition(name, op, val);
        },
        makeBool: (name) => new BoolMediaCondition(name),
        makeEnum: (name, val) => new EnumMediaCondition(name, val)
    }
};
