const aware = require('./aware');

class Range {
    constructor(lo, hi, step = 1) {
        this.lo = lo;
        this.hi = hi;
        this.step = step;
    }
    map(fn) {
        let acc = [];
        for (let n = this.lo; n <= this.hi; n+=this.step) {
            acc.push(fn(n));
        }
        return new IntermediateList(acc);
    }
    get length() {
        return Math.floor((this.hi - this.lo) / this.step);
    }
    lookup(i) {
        const n = this.length;
        if (typeof i !== 'number') {
            throw new Error(`index ${i} is not a number`);
        }
        if (i < 0 || i >= n) {
            throw new Error(`index ${i} is out of range`);
        }
        return this.lo + i * this.step;
    }
}

class List {
    constructor(items) {
        this.items = items;
    }
    map(fn) {
        return new this.constructor(this.items.map(fn));
    }
    reduce(fn, init) {
        return this.items.reduce(fn, init);
    }
    toCommaList() {
        return new CommaList(this.items);
    }
    toSpaceList() {
        return new SpaceList(this.items);
    }
    get length() {
        return this.items.length;
    }
    lookup(i) {
        const n = this.length;
        if (typeof i !== 'number') {
            throw new Error(`index ${i} is not a number`);
        }
        if (i < 0 || i >= n) {
            throw new Error(`index ${i} is out of range`);
        }
        return this.items[i];
    }
}

class IntermediateList extends List {
    toString() {
        throw new Error('conversion of IntermediateList to String is not allowed directly');
    }
}

class CommaList extends List {
    toString() {
        return this.items.join(', ');
    }
}

class SpaceList extends List {
    toString() {
        return this.items.join(' ');
    }
}

module.exports = {
    Range,
    List,
    IntermediateList,
    range: (lo, hi, step) => new Range(lo, hi, step),
    commaList: aware((...xs) => new CommaList(xs)),
    spaceList: aware((...xs) => new SpaceList(xs))
};
