require('colors');

const eq = (failPred, print = JSON.stringify) => (...args) => {
    if (args.length <= 1) throw new Error(' expected at least two arguments for equality matching');
    const getLast = args[args.length - 1];
    const last = getLast();
    for (let i = 0; i < args.length - 1; i++) {
        const item = args[i]();
        if (failPred(last, item)) {
            return {
                ok: false,
                message: `${print(item).red} is not equal to ${print(last).green}`
            };
        }
    }
    return {ok: true};
};

const assert = {
    eq: eq((a, b) => a !== b),
    eq2: eq((a, b) => {
        const math = require('./math');
        return !math.eq(a, b);
    }),
    eqCss: eq(
        (a, b) => {
            const {cssToString} = require('./css');
            const sa = cssToString(a);
            const sb = cssToString(b);
            return typeof sa !== 'string'
                || typeof sb !== 'string'
                || sa !== sb;
        },
        require('./css').cssToString
    ),
    cssPred: (getCss, getBool) => {
        const {cssToString} = require('./css');
        const css = getCss();
        const cssString = cssToString(css);
        if (typeof cssString !== 'string') {
            return {
                ok: false,
                message: 'css was not stringified'
            };
        }
        const stringifiedObject = ({}).toString();
        if (cssString.indexOf(stringifiedObject) !== -1 ||
            cssString.indexOf('css emission error') !== -1) {
            return {
                ok: false,
                message: 'css was stringified incorrectly'
            };
        }
        if (getBool()(cssString)) {
            return {ok: true};
        }
        return {
            ok: false,
            message: 'predicate was not satisfied'.red + ' for\n' + cssString
        };
    },
    bool: (getBool) => {
        if (getBool()) {
            return {ok: true};
        }
        return {
            ok: false,
            message: 'false'.red
        };
    }
};

let parentFrame = null;

const report = (rootFrame) => {
    let indent = '';
    let acc = '';
    const withIndent = (x) => {
        const lines = x.split('\n');
        if (lines.length > 1) {
            return lines
                    .map(x => indent + x)
                    .join('\n');
        }
        return indent + x;
    };

    const log = (x) => acc += '\n' + withIndent(x);

    const reportR = (frame) => {
        const oldIndent = indent;
        indent = indent + '  ';
        switch (frame.type) {
            case 'globalRoot':
                frame.children.forEach(reportR);
                break;
            case 'fileRoot':
            case 'test':
                if (frame.stats.total === 0) break;
                const color = frame.stats.total !== frame.stats.ok ? 'red' : 'green';

                log(`${frame.name} (${frame.stats.ok.toString()[color]}/${frame.stats.total})`);
                if (frame.stats.total !== frame.stats.ok) {
                    frame.children.forEach(reportR);
                }
                break;
            case 'assertion':
                if (frame.result.ok) {
                    log(`${frame.name} ${'ok'.green}`);
                } else if (frame.error) {
                    log(`${frame.name} thrown an error: ${frame.error.message.red}`);
                } else {
                    log(`${frame.name} ${'failed'.red}: ${frame.result.message}`);
                }
                break;
        }
        indent = oldIndent;
    };

    reportR(rootFrame);

    return acc;
};

const addInnerStats = (rootFrame) => {
    const addInnerStatsR = (frame) => {
        switch (frame.type) {
            case 'globalRoot':
            case 'fileRoot':
            case 'test':
                frame.stats = {
                    total: 0,
                    ok: 0,
                    failed: 0,
                    errors: 0
                };
                frame.children.forEach((childFrame) => {
                    addInnerStatsR(childFrame);
                    frame.stats.total += childFrame.stats.total;
                    frame.stats.ok += childFrame.stats.ok;
                    frame.stats.failed += childFrame.stats.failed;
                    frame.stats.errors += childFrame.stats.errors;
                });
                break;
            case 'assertion':
                frame.stats = {
                    total: 1,
                    ok: frame.result.ok ? 1 : 0,
                    failed: !frame.error && !frame.result.ok ? 1 : 0,
                    errors: frame.error ? 1 : 0
                };
                break;
        }
    };

    addInnerStatsR(rootFrame);

    return rootFrame;
};

module.exports = {
    addInnerStats,
    report,
    makeGlobalTestRoot() {
        return {type: 'globalRoot', children: []};
    },
    makeFileTestRoot(name) {
        return {name, type: 'fileRoot', children: []};
    },
    test(name, fn, cb, only=false) {
        const frame = {only, type: 'test', name, children: []};
        const oldParentFrame = parentFrame;
        parentFrame = frame;
        try {
            fn();
        } finally {
            parentFrame = oldParentFrame;
        }

        if (parentFrame) {
            parentFrame.children.push(frame);
        } else {
            cb(frame);
        }
    },
    assert(type, name, expressions, only=false) {
        const assertionReport = {only, type: 'assertion', name, result: {}};
        if (typeof assert[type] !== 'function') {
            throw new Error(`no such assertion type ${type}`);
        }
        try {
            assertionReport.result = assert[type].apply(null, expressions);
        } catch (e) {
            assertionReport.error = e;
        }
        parentFrame.children.push(assertionReport);
    }
};
