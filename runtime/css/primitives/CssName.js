const CssColor = require('./CssColor');

module.exports = class CssName {
    constructor(x) {
        console.assert(typeof x === 'string', 'CssName should contain a string');
        this.value = x;
    }
    asColor() {
        return CssColor.fromString(this, this.value);
    }
    toString() {
        return this.value;
    }
    eq(cssName) {
        return this.value === cssName.value;
    }
};
