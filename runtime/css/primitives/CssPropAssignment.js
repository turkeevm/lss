const mediaAware = require('../../aware');

module.exports = class CssPropAssignment {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }

    bubbleUpMedia() {
        return mediaAware.conjMap(
            this.name,
            this.value,
            (name, value) => new CssPropAssignment(name, value)
        ) || this;
    }
};
