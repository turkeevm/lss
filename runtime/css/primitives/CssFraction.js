module.exports = class CssFraction {
    constructor(nom, den) {
        this.nom = nom;
        this.den = den;
    }
    toString() {
        return `${this.nom}/${this.den}`;
    }
};
