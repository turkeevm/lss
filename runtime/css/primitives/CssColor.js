const Color = require('color');

module.exports = class CssColor {
    constructor(origin, color) {
        this.origin = origin;
        this.color = color;
    }
    toString() {
        if (this.origin) {
            return this.origin.toString();
        }
        if (this.color.model === 'rgb' && this.color.valpha === 1) {
            return this.color.hex();
        }
        return this.color.round().string();
    }

    static fromString(origin, colorString) {
        return new CssColor(origin, new Color(colorString));
    }

    static fromColor(origin, color) {
        return new CssColor(origin, color);
    }
};
