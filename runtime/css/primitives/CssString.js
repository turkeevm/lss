module.exports = class CssString {
    constructor(x) {
        this.value = x;
    }
    toString() {
        return JSON.stringify(this.value);
    }
};
