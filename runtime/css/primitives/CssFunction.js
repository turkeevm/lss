const CssColor = require('./CssColor');

class CssFunction {
    constructor(name, args) {
        this.name = name;
        this.args = args;
    }
    toString() {
        return `${this.name}(${this.args})`;
    }
}

const cssFunction = (name, args) => {
    const f = new CssFunction(name, args);
    if (/^(rgb|hsl)a?$/.test(name.value)) {
        return CssColor.fromString(f, f.toString());
    }
    return f;
};

module.exports = cssFunction;
