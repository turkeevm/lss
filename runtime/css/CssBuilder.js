const CssInline = require('./frames/CssInline');
const CssRule = require('./frames/CssRule');

const CssPropAssignment = require('./primitives/CssPropAssignment');

module.exports = class CssBuilder {
    constructor() {
        this.frame = new CssInline();
    }

    enterSelector(selector, mapping) {
        const childFrame = new CssRule(selector, this.frame, mapping);
        this.frame.children.push(childFrame);
        this.frame = childFrame;
    }

    exitSelector() {
        this.frame = this.frame.parent;
    }

    prop(propName, propValue) {
        this.frame.children.push(new CssPropAssignment(propName, propValue));
    }

    injectMixin(mixin) {
        if (mixin instanceof CssInline) {
            this.frame.children.push(...mixin.children);
        } else {
            this.frame.children.push(mixin.cloneWithParent(this.frame));
        }
    }
};
