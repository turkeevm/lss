const CssBuilder = require('./CssBuilder');

const CssInline = require('./frames/CssInline');
const CssRule = require('./frames/CssRule');

const CssPropAssignment = require('./primitives/CssPropAssignment');
const CssName = require('./primitives/CssName');
const CssColor = require('./primitives/CssColor');
const CssString = require('./primitives/CssString');
const makeCssFunction = require('./primitives/CssFunction');
const CssFraction = require('./primitives/CssFraction');

const KeyframesBuilder = require('./KeyframesBuilder');

const aware = require('../aware');

module.exports = {
    builder: () => new CssBuilder(),
    keyframesBuilder: (name) => new KeyframesBuilder(name),

    CssInline,
    CssRule,
    CssPropAssignment,

    cssToString: require('./stringifier'),

    makeCssString: aware((x) => new CssString(x)),
    makeCssName: aware((x) => new CssName(x)),
    makeColor: (hex) => CssColor.fromString(null, hex),
    makeCssFunction,
    makeFraction: aware((a, b) => new CssFraction(a, b))
};


/**
 *
 * css {
 *   .a {
 *     b: c;
 *     +d();
 *   }
 * }
 *
 * (() => {
 *   $builder = $css.builder();
 *     $builder.enterSelector('.a');
 *       $builder.prop('b', $css.string('c'));
 *       $builder.injectMixin(d());
 *     $builder.exitSelector();
 *   return $builder.frame;
 * }())
 *
 **/

