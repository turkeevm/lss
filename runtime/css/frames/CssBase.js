const CssPropAssignment = require('../primitives/CssPropAssignment');
const keyBy = require('lodash/keyBy');

module.exports = class CssBase {
    constructor() {
        this.children = [];
        this.nameIdx = {};
        this.hasNameIdx = false;
    }
    transferTo(target) {
        target.children = this.children;
        target.nameIdx = this.nameIdx;
        target.hasNameIdx = this.hasNameIdx;
    }
    lookup(name) {
        this.ensureNameIdx();
        if (this.nameIdx.hasOwnProperty(name)) {
            return this.nameIdx[name].value;
        }
        throw new Error(`there is no property "${name}" in ${this.report}`);
    }
    ensureNameIdx() {
        if (!this.hasNameIdx) {
            this.nameIdx = keyBy(this.children.filter(x => x instanceof CssPropAssignment), 'name');
            this.hasNameIdx = true;
        }
    }
};
