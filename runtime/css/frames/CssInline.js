const CssBase = require('./CssBase');

module.exports = class CssInline extends CssBase {
    get report() {
        return '#CssInline#';
    }
};
