const CssBase = require('./CssBase');
const memoize = require('lodash/memoize');
const mediaAware = require('../../aware');

const genId = memoize(() => {
    let nextIdForName = 1;
    return {
        count: () => nextIdForName - 1,
        id: memoize(() => nextIdForName++)
    };
});

module.exports = class KeyframesList extends CssBase {
    constructor(name) {
        super();
        this.name = name;
    }

    precount() {
        return this.id;
    }

    get id() {
        return this.name + '_' + this.index;
    }

    get index() {
        return genId(this.name).id(this.renderContent());
    }

    get fullName() {
        return genId(this.name).count() === 1
            ? this.name
            : `${this.name}_${this.index}`;
    }

    bubbleUpMedia() {
        const children = mediaAware.array(this.children.map(child => child.bubbleUpMedia()));
        const name = this.name;
        return mediaAware.conjMap(
                children,
                name,
                (children, name) => {
                    const tmp = new KeyframesList(name);
                    tmp.children = children;
                    return tmp;
                }
            ) || this;
    }

    //todo: optimize. Should only be called once per instance per render (mobx?)
    renderContent() {
        return this.children
            .map(child => child.render()
                .split('\n')
                .map(x => `  ${x}`)
                .join('\n'))
            .join('\n');
    }

    render() {
        return [
            `@keyframes ${this.fullName} {`,
            this.renderContent(),
            `}`
        ].join('\n');
    }

    get report() {
        return '#KeyframesList#';
    }
};
