const CssBase = require('./CssBase');

module.exports = class CssRule extends CssBase {
    constructor(selector, parent, mapping) {
        super();
        this.parent = parent;
        this.selector = selector;
        this.mapping = mapping;
    }
    cloneWithParent(parent) {
        const tmp = this.clone();
        tmp.parent = parent;
        return tmp;
    }

    clone() {
        const tmp = new CssRule(this.selector, this.parent);
        tmp.children = this.children.map(x => {
            if (x instanceof CssRule) return x.clone();
            return x;
        });
        return tmp;
    }

    get report() {
        return '#CssRule#';
    }

};
