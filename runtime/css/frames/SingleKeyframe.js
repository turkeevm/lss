const CssBase = require('./CssBase');
const mediaAware = require('../../aware');

module.exports = class SingleKeyframe extends CssBase {
    constructor(percentage, parent) {
        super();
        this.percentage = percentage;
        this.parent = parent;
    }

    render() {
        return [
            `${this.percentage} {`,
            ...this.children.map(({name, value}) => `  ${name}: ${value};`),
            `}`
        ].join('\n');
    }

    bubbleUpMedia() {
        const children = mediaAware.array(this.children.map(child => child.bubbleUpMedia()));
        const percentage = this.percentage;
        return mediaAware.conjMap(
            children,
            percentage,
            (children, percentage) => {
                const tmp = new SingleKeyframe(percentage, this.parent);
                tmp.children = children;
                return tmp;
            }
        ) || this;
    }
};
