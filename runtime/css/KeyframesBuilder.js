const {typeInfo} = require('../util');

const KeyframesList = require('./frames/KeyframesList');
const SingleKeyframe = require('./frames/SingleKeyframe');
const CssInline = require('./frames/CssInline');

const CssPropAssignment = require('./primitives/CssPropAssignment');

module.exports = class KeyframesBuilder {
    constructor(name) {
        this.frame = new KeyframesList(name);
    }

    enterKeyframe(percentage) {
        const childFrame = new SingleKeyframe(percentage, this.frame);
        this.frame.children.push(childFrame);
        this.frame = childFrame;
    }

    exitKeyframe() {
        this.frame = this.frame.parent;
    }

    prop(propName, propValue) {
        this.frame.children.push(new CssPropAssignment(propName, propValue));
    }

    injectMixin(mixin) {
        if (this.frame instanceof KeyframesList) {
            if (mixin instanceof KeyframesList) {
                return this.frame.children.push(...mixin.children);
            }
            if (mixin instanceof SingleKeyframe) {
                return this.frame.children.push(mixin.cloneWithParent(this.frame));
            }
        }
        if (this.frame instanceof SingleKeyframe) {
            if (mixin instanceof CssPropAssignment) {
                return this.frame.children.push(mixin);
            }
            if (mixin instanceof CssInline) {
                return this.frame.children.push(...mixin.children);
            }
        }
        throw new Error(`cannot inject ${typeInfo(mixin)} as mixin into ${typeInfo(this.frame)}`);
    }
};
