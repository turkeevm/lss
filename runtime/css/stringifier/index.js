const CssInline = require('../frames/CssInline');
const CssRule = require('../frames/CssRule');
const KeyframesList = require('../frames/KeyframesList');
const CssAssignment = require('../primitives/CssPropAssignment');
const {List} = require('../../lists');

const MediaValue = require('../../media/MediaValue');
const MediaCondition = require('../../media/conditions/MediaCondition');

const aware = require('../../aware');

const {typeInfo} = require('../../util');

const {AtomRule, ShallowRule} = require('./primitives');

const {SourceNode} = require('source-map');
const {locationFor} = require('../../../lang/generator/primitives');

const {seq, atom, propAssignment} = aware;

const addAtomSig = seq(atom, atom, propAssignment, atom);
const flattenerSig = seq(atom, atom, atom, atom);
const awareFlattener = (signature, f) => (cond, selector, x, mapping) => {
    const {MediaValue} = require('../../media');
    const mArgs = signature([cond, selector, x, mapping]);
    if (mArgs instanceof MediaValue) {
        return mArgs.map(([_, selector, x, mapping], cond) => f(
            cond,
            selector,
            x,
            mapping
        ));
    }
    return f(cond, selector, x, mapping);
};

const combineSelectors = aware((parent, child) => {
    if (!parent) return child;
    if (!/&/.test(child)) return parent + ' ' + child;
    return child.replace(/&/g, parent);
});

const flattenRules = (rootFrame) => {
    const acc = [];

    const flattenList = awareFlattener(flattenerSig, (parentCond, parentSelector, children, mapping) =>
        children.forEach(child => flattenOne(parentCond, parentSelector, child, mapping)));

    const flattenOne = (parentCond, parentSelector, child, mapping) => {
        if (child instanceof CssInline) {
            return flattenList(parentCond, parentSelector, child.children, mapping);
        }
        if (child instanceof CssRule) {
            return flattenList(
                parentCond,
                combineSelectors(parentSelector, child.selector),
                child.children,
                child.mapping
            );
        }
        if (child instanceof MediaValue) {
            return child.forEach((caseChild, caseCond) => {
                return flattenOne(
                    MediaCondition.conj(parentCond, caseCond),
                    parentSelector,
                    caseChild,
                    mapping
                );
            });
        }
        if (child instanceof CssAssignment) {
            if (child.value instanceof KeyframesList) {
                child.value = child.value.bubbleUpMedia();
                aware.awareApply(
                    (keyframes) => keyframes.precount(),
                    [child.value]
                );
            }
            return addAtom(
                parentCond,
                parentSelector,
                child,
                mapping
            );
        }
        throw new Error(`unexpected entity ${typeInfo(child)} in css expression`)
    };

    const addAtom = awareFlattener(addAtomSig, (cond, selector, assignment, mapping) => {
        acc.push(new AtomRule(cond, selector, assignment, mapping));
    });

    if (rootFrame instanceof CssInline) {
        flattenList(true, '', rootFrame.children, null);
    } else if (rootFrame instanceof List) {
        flattenList(true, '', rootFrame.items, null);
    } else if (rootFrame instanceof MediaValue) {
        rootFrame.forEach((rootCase, condition) => {
           const flatCase = flattenRules(rootCase);
           flatCase.forEach(atom => acc.push(atom.conjCondition(condition)));
        });
    }

    return acc;
};

const groupRules = (atoms) => {
    if (atoms.length === 0) {
        return [];
    }

    // выпиливаем перекрытия по MediaCondition
    atoms.forEach((atom, i) => {
        if (atom === null) {
            return;
        }

        let currentPos = i;
        let currentAtom = atom;
        for (let j = i + 1; j < atoms.length; j++) {
            const testedAtom = atoms[j];
            if (testedAtom === null) continue;
            if (currentAtom.selector !== testedAtom.selector) break;
            if (!MediaCondition.covers(testedAtom.media, currentAtom.media)) continue;
            if (testedAtom.assignment.name === currentAtom.assignment.name) {
                atoms[currentPos] = null;
                currentPos = j;
                currentAtom = testedAtom;
            }
        }
    });

    let filteredAtoms = atoms.filter(x => x !== null);

    //группируем то что осталось и выпиливаем перекрытия по свойствам
    let acc = [];
    filteredAtoms.forEach((atom, i) => {
        if (atom === null) {
            return;
        }
        const currentRule = new ShallowRule(atom.media, atom.selector, [atom.assignment], atom.mapping);
        for (let j = i + 1; j < filteredAtoms.length; j++) {
            const joinedAtom = filteredAtoms[j];
            if (!joinedAtom) {
                continue;
            }
            if (currentRule.selector !== joinedAtom.selector) {
                break;
            }
            if (!MediaCondition.isEqual(currentRule.media, joinedAtom.media)) {
                // дальше может попасться годный атом (с учётом conj)
                continue;
            }
            let k = 0;
            //todo: делать вот это только в "оптимизирующем режиме"
            //todo: выпиливать перекрытия вида border-image < border
            for (;k < currentRule.assignments.length; k++) {
                if (currentRule.assignments[k].name === joinedAtom.assignment.name) {
                    currentRule.assignments[k].value = joinedAtom.assignment.value;
                    break;
                }
            }
            if (k === currentRule.assignments.length) {
                currentRule.assignments.push(joinedAtom.assignment);
            }
            filteredAtoms[j] = null;
        }
        acc.push(currentRule);
    });

    return acc;
};

const stringifyRules = (shallowRules) => {
    const mentionedKeyframes = {};
    const stringifyPropValue = val => {
        if (val instanceof KeyframesList) {
            if (!mentionedKeyframes[val.id]) {
                mentionedKeyframes[val.id] = val;
            }
            return val.fullName;
        }

        return val;
    };

    const normalRules = shallowRules
        .map(rule => {
            const ruleLines = [
                `${rule.selector} {`,
                ...rule.assignments.map(({name, value}) =>
                `  ${name}: ${stringifyPropValue(value)};`),
                `}`
            ];

            if (rule.media === true) return ruleLines.join('\n');
            if (rule.media === false) return null;
            return [
                `@media ${rule.media} {`,
                ...ruleLines.map(line => `  ${line}`),
                `}`
            ].join('\n');
        })
        .filter(x => x !== null)
        .join('\n\n');

    const keyframesCode = Object.keys(mentionedKeyframes)
        .map(id => mentionedKeyframes[id].render())
        .filter(x => x)
        .join('\n\n');

    return [keyframesCode, normalRules]
        .filter(x => x)
        .join('\n\n');
};



const interpose = (xs, sep) => {
    if (xs.length === 0) return xs;
    const acc = [xs[0]];
    for (let i = 1; i < xs.length; i++) {
        acc.push(sep, xs[i]);
    }
    return acc;
};

const nodeJoin = (xs, sep) => {
    const res = new SourceNode();
    res.add(interpose(xs, sep));
    return res;
};

const stringifyRulesWithSourceMap = (shallowRules, modules) => {
    const withPos = f => shallowRule => {
        const sourceNode = f(shallowRule);
        if (!sourceNode) return null;
        const {line, column} = locationFor(
            shallowRule.mapping.start,
            modules[shallowRule.mapping.source].lssCode
        );

        if (!column) {
            debugger;
        }
        sourceNode.line = line;
        sourceNode.column = column;
        sourceNode.source = shallowRule.mapping.source;
        return sourceNode;
    };

    const mentionedKeyframes = {};
    const stringifyPropValue = val => {
        if (val instanceof KeyframesList) {
            if (!mentionedKeyframes[val.id]) {
                mentionedKeyframes[val.id] = val;
            }
            return val.fullName;
        }

        return val;
    };

    const normalRules = nodeJoin(
        shallowRules
            .map(withPos(rule => {
                const ruleLines = [
                    `${rule.selector} {`,
                    ...rule.assignments.map(({name, value}) =>
                        `  ${name}: ${stringifyPropValue(value)};`),
                    `}`
                ];

                if (rule.media === true) return nodeJoin(ruleLines, '\n');
                if (rule.media === false) return null;
                return nodeJoin([
                    `@media ${rule.media} {`,
                    ...ruleLines.map(line => `  ${line}`),
                    `}`
                ], '\n');
            }))
            .filter(x => x !== null),
        '\n\n'
    );

    const keyframesCode = nodeJoin(
        Object.keys(mentionedKeyframes)
            .map(id => mentionedKeyframes[id].render())
            .filter(x => x),
        '\n\n'
    );

    return nodeJoin(
        [keyframesCode, normalRules].filter(x => x),
        '\n\n'
    );
};

module.exports = (inlineFrame, modules) => {
    if (modules) {
        AtomRule.enableDbg();
    }

    const flatRules = flattenRules(inlineFrame);
    const grouppedRules = groupRules(flatRules);

    if (modules) {
        AtomRule.enableDbg();

        const rootNode = stringifyRulesWithSourceMap(grouppedRules, modules);

        const mapped = rootNode.toStringWithSourceMap();

        const mapsString = mapped.map.toString();
        const mapsBase64 = new Buffer(mapsString).toString('base64');
        const comment = `/*# sourceMappingURL=data:application/json;charset=utf-8;base64,${mapsBase64}*/`;


        return mapped.code + '\n' + comment;
    }

    return stringifyRules(grouppedRules);
};
