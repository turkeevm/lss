
let dbgMode = false;

class AtomRule {
    static enableDbg() {
        dbgMode = true;
    }
    constructor(media, selector, assignment, mapping) {
        if (dbgMode && !mapping) {
            debugger;
        }
        this.media = media;
        this.selector = selector;
        this.assignment = assignment;
        this.mapping = mapping;
    }

    conjCondition(condition) {
        const MediaCondition = require('../../media/conditions/MediaCondition');

        return new AtomRule(
            MediaCondition.conj(this.media, condition),
            this.selector,
            this.assignment
        );
    }
}

class ShallowRule {
    constructor(media, selector, assignments, mapping) {
        if (dbgMode && !mapping) {
            debugger;
        }
        this.media = media;
        this.selector = selector;
        this.assignments = assignments;
        this.mapping = mapping;
    }
}

module.exports = {
    AtomRule, ShallowRule
};
