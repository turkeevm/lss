const resolve = require('resolve');
const path = require('path');

module.exports = {
    initialContext: process.cwd(),
    resolve: (fileName, context) => {
        const resolvedFileName = resolve.sync(fileName, {basedir: context, extensions: ['.xcss']});
        return {
            fileName: resolvedFileName,
            context: path.dirname(resolvedFileName)
        };
    }
};
