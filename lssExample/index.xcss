import defs from './util/defs';
import './testImport';
import './testMath';
import './testMedia';
import './testColor';

test 'css rule' {
  def x = css {
    selector {
      property: literal;
      number: 1;
      simple-nested {
        b: 2;
      }

      &__ref-nested {
        c: 3;
      }
    }
  };
  assert('cssPred', 'generates simple selector', x, (x) => testRe('selector', x));
  assert('cssPred', 'generates property names', x, (x) => testRe('property', x) && testRe('number', x));
  assert('cssPred', 'generates css names', x, (x) => testRe('literal', x));
  assert('cssPred', 'generates numbers', x, (x) => testRe(': 1;', x));
  assert('cssPred', 'generates simple nested', x, (x) => testRe('selector simple-nested', x));
  assert('cssPred', 'generates referenced nested', x, (x) => testRe('selector__ref-nested', x));
};

test 'cond' {
  def y = 35em;
  assert('eq2', 'match regular case',
    cond {
      y > 20em: 30px;
      y > 30em: 40px;
      else: 50px;
    },
    30px
  );

  assert('eq2', 'match else case',
    cond {
      y < 20em: 30px;
      y < 30em: 40px;
      else: 50px;
    },
    50px
  );
};

test 'property access' {
  test 'via dot' {
    def obj = {
      a: 1,
      b: {
        c: 2
      },
      d: () => 3
    };
    assert('eq', 'simple', obj.a, 1);
    assert('eq', 'deep', obj.b.c, 2);
    assert('eq', 'call', obj.d(), 3);
  }

  test 'computed' {
    assert('eqCss', 'call function via computed name',
      `{ .a { +(defs['absFit'](10px, 20px)); }},
      `{ .a { +(defs.absFit(10px, 20px)); }}
    );
    assert('eq', 'nested property', defs['point']['y'], defs.point.y);
    assert('eq', 'via expression', defs['po' + 'int']['y'], defs.point.y);
  }

  test 'in css' {
    assert('eq2', 'inline', defs.absFit().position, `absolute);
  }

};

test 'range' {
  def xs = [0:3].map(x => 1px * x);
  def ys = [0:2:6].map(x => 1px * x);
  assert('eqCss', 'from-to',
    `{ .a { a: $(xs.toSpaceList()); }},
    `{ .a { a: 0px 1px 2px 3px; }}
  );
  assert('eqCss', 'from-step-to',
    `{ .a { a: $(ys.toSpaceList()); }},
    `{ .a { a: 0px 2px 4px 6px; }}
  );
  assert('eq', '[0:10][2] == 2', [0:10][2], 2);
};

test 'css functions introspection' {
  assert('eq2', '`translateX(10px).args == 10px', `translateX(10px).args, 10px);
};

test 'block statement' {
  def b = {
      def x = 1px;
      def y = 2px;
      `translate($x, $y);
  };

  assert('eqCss', 'is working',
    `{ .a { a: $b; }},
    `{ .a { a: translate(1px, 2px); }}
  );
};

def rowHeight = 25px;
defunit rw = (x) => x * rowHeight;

test 'custom units' {
  assert('eq2', 'are working', 1rw, 25px);
};

test 'map' {
  assert('eqCss', '[1:3].map((i) => css ...',
    [1:3].map((i) => css { .test-$(i) { padding: $(i * 10px); } }),
    for (i = 1..3) css { .test-$(i) { padding: $(i * 10px); } },
    css {
      .test-1 { padding: 10px; }
      .test-2 { padding: 20px; }
      .test-3 { padding: 30px; }
    }
  );
};

/*
  block comment
*/

//line comment

test 'mixins' {
  assert('eqCss', 'are working',
    `{ .a { +(() => `{ b: 1px; c: 2px; })(); }},
    `{ .a { b: 1px; c: 2px; }}
  );
};

test 'interpolation' {
  assert('eq', 'strings', './images/bg-$(10 + 5).png', './images/bg-15.png');
};

test '&-reference' {
  assert('eqCss', 'simple case',
    `{ .a { &__b { c: 1px; }}},
    `{ .a__b { c: 1px; }}
  );

  assert('eqCss', 'preudo-class',
    `{ .a { &:hover { c: 1px; }}},
    `{ .a:hover { c: 1px; }}
  );
};

def testFraction = `(10px / 20px);
def testFractionCss = css {
    .test-fraction {
        font: $testFraction;
        border-radius: 10px 20px / 30px 40px;
    }
};

test 'self reference' {
  assert('eqCss', 'is working',
    `{ .test-self-ref {
      width: 20px;
      height: ^(self.width / 2);
    }},
    `{ .test-self-ref {
          width: 20px;
          height: 10px;
    }}
  );
};

test 'animation' {
  def x = () => {
    def a = keyframes abc {
      0% { height: 10px; }
      100% { color: black; }
    };

    def c = keyframes qwe {
      0% { height: 5px; }
      100% { color: red; }
    };

    def d = keyframes zxc {
      0% { height: $(media {
        (true): 10px;
        width >= 320px: 20px;
      }); }
      100% { color: red; }
    };

    def e = (a) => keyframes fff {
      0% {height: $a; }
    };

    def h = keyframes test-lazy {
      0% {
        width: 2px;
        test-lazy: ^(self.width * 2);
      }
    };

    css {
      .test-animation-1 {
        a-a: $a;
        a-b: $a;
        a-c: $c;
        a-d: $d;
        a-e: $(e(1px));
        a-f: $(e(2px));
        a-g: $(e(1px));
        a-h: $h;
      }
    };
  };

  assert('cssPred', 'produces keyframes', x(), (code) => testRe('@keyframes abc', code));
  assert('cssPred', 'produces multiple keyframes', x(), (code) => testRe('@keyframes qwe', code));
  assert('cssPred', 'does not break keyframes content', x(),
    (code) => testRe('0%', code)
           && testRe('100%', code)
           && testRe('height: 10px;', code)
           && testRe('color: black;', code)
  );

  assert('cssPred', 'has valid name in property position', x(), (code) => testRe('a-a: abc;', code));
  assert('cssPred', 'does not rename trivial cases', x(), (code) => testRe('a-b: abc;', code) && testRe('a-a: abc;', code));

  assert('cssPred', 'renames media * keyframes', x(),
    (code) => testRe('a-d: zxc_1', code)
           && testRe('a-d: zxc_2', code)
  );

  assert('cssPred', 'renames are dynamically generated', x(),
    (code) => testRe('a-e: fff_1', code)
           && testRe('a-f: fff_2', code)
  );

  assert('cssPred', 'does not rename if generated keyframes are the same', x(),
    (code) => testRe('a-g: fff_1', code)
  );

  assert('cssPred', 'works with lazy', x(),
    (code) => testRe('test-lazy: 4px', code)
  );
};

test 'functions' {
    assert('eq', 'are working',
        1,
        (() => 1)()
    );
    assert('eq', 'can have blockExpression as its body',
        1,
        (() => { def x = 1; x; })()
    );
};


def color = `yellow;
def m1 = media {
  (true): 10px;
  height >= 768px: 20px;
  height >= 1024px: 30px;
};

def m2 = media {
  (true): 100px;
  width >= 1024px: 200px;
  width >= 1280px: 300px;
};


emit css {
  body {
    background: red;
    &.green {
      background: green;
    }
    $('.' + color.toString())-trololo {
      background: $color;
      width: $m1;
      height: $(m1 + m2);
    }
  }
};
