const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const parse = require('./lang/parser');
const {stats} = require('./lang/parser/util');
const resolver = require('./resolverExample');
const generate = require('./lang/generator/index');
const {chunks2code} = require('./lang/generator/chunks2code');
const analyze = require('./lang/analyse');
const now = require('performance-now');
const {cssToString} = require('./runtime/css');

let parseTime = 0;
let loadTime = 0;

const loadAll = (entryPoint) => {
    const modules = {};

    const stack = [];

    const loadAllR = (entryPoint, entryContext) => {
        const {context, fileName} = resolver.resolve(entryPoint, entryContext);
        const beforeLoad = now();
        const lssCode = fs.readFileSync(fileName, 'utf8');
        loadTime += now() - beforeLoad;
        
        const beforeParse = now();
        const ast = parse(lssCode);
        parseTime += now() - beforeParse;

        if (modules[fileName]) {
            if (modules[fileName].status === 'loading') {
                modules[fileName].status = 'circular';
                modules[fileName].circularStack = stack.slice(0);
            }
            return fileName;
        }

        modules[fileName] = {
            lssCode: new String(lssCode),
            entryPoint,
            status: 'loading',
            ast,
            dependencies: []
        };

        stack.push(entryPoint);
        const dRefs = extractDependencies(ast);
        const refToFile = {};
        modules[fileName].dependencies = dRefs.map((dRef) => {
            const dFileName = loadAllR(dRef, context);
            refToFile[dRef] = dFileName;
            return dFileName;
        });

        renameDependencies(ast, refToFile);
        modules[fileName].status = 'ok';
        stack.pop();

        return fileName;
    };

    const getReferenceName = (importAst) => {
        const referenceInterpolationNode = importAst.value[1];
        const firstChunkNode = referenceInterpolationNode.value[0];
        return firstChunkNode.value;
    };

    const setReferenceName = (importAst, newRef) => {
        const referenceInterpolationNode = importAst.value[1];
        const firstChunkNode = referenceInterpolationNode.value[0];
        firstChunkNode.value = newRef;
    };

    const extractDependencies = (ast) => ast
        .filter(({type}) => type === 'importStatement')
        .map(getReferenceName);

    const renameDependencies = (ast, renames) => ast
        .forEach(statement => {
            if (statement.type === 'importStatement') {
                const dRef = getReferenceName(statement);
                setReferenceName(statement, renames[dRef]);
            }
        });

    const entryPointFileName = loadAllR(entryPoint, resolver.initialContext);

    return {entryPointFileName, modules};
};

const process = () => {
    parseTime = 0;
    loadTime = 0;
    const entryPoint = './lssExample/index.xcss';
    const {entryPointFileName, modules} = loadAll(entryPoint);
    console.log(`load: ${loadTime.toFixed(3)}ms`);
    console.log(`parse: ${parseTime.toFixed(3)}ms`);

    console.time('analyze');
    const transformedModules = _.mapValues(modules, (module) => {
        return Object.assign({}, module, {
            ast: analyze(module.ast)
        });
    });
    console.timeEnd('analyze');

    console.time('codeChunks');
    const jsCodeChunks = generate(entryPointFileName, transformedModules);
    console.timeEnd('codeChunks');

    console.time('generate');
    const jsCode = chunks2code(jsCodeChunks, modules);
    console.timeEnd('generate');

    console.time('eval');
    require('./runtime/init')();
    const result = eval(jsCode);
    console.timeEnd('eval');

    result.modules = modules;

    return result;
};

const N = 1;
const logResult = true;
const logParserStats = false;
const results = new Array(N).fill(0).map((_, i) => {
    console.log(`pass #${i + 1}:`);
    console.time('total');
    const res = process();
    console.timeEnd('total');
    console.log('\n');

    return res;
});


/*
if (results[0].css !== results[results.length - 1].css) {
    throw new Error('different results from sequential passes');
}
*/

const test = require('./runtime/test');

if (logResult) {
    const rest = _.omit(results[0], ['css', 'testReport', 'modules']);

    const rawTestData = results[0].testReport;
    const withStats = test.addInnerStats(rawTestData);

    console.log(test.report(withStats));

    if (!_.isEmpty(rest)) {
        console.log('\n\nexports: ');
        console.log(rest);
    }
}

if (logParserStats) {
    console.log(stats);
}


fs.writeFileSync('./lssExample/html/c.css', cssToString(results[0].css.frame, results[0].modules));
