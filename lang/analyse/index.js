const _ = require('lodash');

const dimensions = _.keyBy([
    'width',
    'height',
    'aspect-ratio',
    'color',
    'monochrome',
    'resolution'
], x => x);

const mediaTypes = _.keyBy([
    'all',
    'aural',
    'braille',
    'handheld',
    'print',
    'projection',
    'screen',
    'tty',
    'tv',
    'embossed',
    'speech',
    'grid'
], x => x);

const enumConditions = {
    scan: {progressive: true, interlace: true},
    orientation: {portrait: true, landscape: true}
};

class Context {
    constructor(parent = null, content = {}) {
        this.parent = parent;
        this.ownContent = content;
    }
    lookup(name) {
        return this.hostFor(name).ownContent[name];
    }
    hostFor(name) {
        if (name in this.ownContent) return this;
        if (!this.parent) throw new Error(`there is no "${name}" in context`);
        return this.parent.hostFor(name);
    }
    update(name, f) {
        const host = this.hostFor(name);
        host.ownContent[name] = f(host.ownContent[name]);
    }
    extend(defs) {
        return new Context(this, defs);
    }
    report(field) {
        console.log(this.ownContent[field]);
        this.parent && this.parent.report(field);
    }
}

class Flatten {
    constructor(node) {
        this.node = node;
    }
}

const analyzeModule = (astRoot) => {
    let currentContext = new Context(null, {
        node: 'module',
        names: _.fromPairs(
            Object.keys(require('../../runtime/stdlib'))
                .map(x => [x, true])
        )
    });

    const contextScope = (defs, f) => {
        currentContext = currentContext.extend(defs);
        const res = f();
        currentContext = currentContext.parent;
        return res;
    };

    const analyzeNode = (ast) => {
        if (typeof ast === 'undefined') {
            currentContext.report('node');
            throw new Error('wtf: undefined node?');
        }
        if (typeof ast !== 'object' || typeof ast.type !== 'string') {
            return ast;
        }

        const nodeName = ast.type;
        if (typeof nodeName !== 'string') {
            console.log('analyzer stack:');
            currentContext.report('node');
            throw new Error(`cannot analyze nodeName=${JSON.stringify(nodeName)}`);
        }
        const result = contextScope({node: nodeName}, () => {
            const analyzer = analyzers[nodeName] || normalAnalyzer;
            return analyzer(ast.value);
        });

        if (result instanceof Flatten) {
            return {...ast, ...analyzeNode(result.node)};
        }
        return {...ast, type: nodeName, value: result};
    };

    const normalAnalyzer = (args) =>
        args instanceof Array
            ? args.map(analyzeNode)
            : analyzeNode(args);

    const analyzers = {
        defStatement: ([name, value]) => {
            const names = currentContext.lookup('names');
            if (name.value in names) {
                throw new Error(`${name.value} is already declared in current scope`);
            }
            names[name.value] = true;
            return [analyzeNode(name), analyzeNode(value)];
        },
        blockExpression: ([context, value]) => {
            currentContext.ownContent.names = {};
            return [context.map(analyzeNode), analyzeNode(value)];
        },
        lambdaExpression: ([args, body]) => {
            const nDefault = countDefaultArgs();
            const argsA = args.map(analyzeNode);
            currentContext.names = argsA.reduce((names, arg) => {
                names[arg.value] = true;
                return names;
            }, {});

            const bodyA = (() => {
                const {type: bodyNode, value: bodyContent} = body;
                if (bodyNode === 'blockExpression') {
                    return {
                        type: 'blockExpressionContent',
                        value: analyzers.blockExpression(bodyContent)
                    };
                }
                if (bodyNode === 'objectLiteral') {
                    return {
                        type: 'objectLiteralContent',
                        value: analyzers.objectLiteral(bodyContent)
                    };
                }
                return {
                    type: 'simpleReturn',
                    value: analyzeNode(body)
                };
            })();

            return [nDefault, argsA, bodyA];

            function countDefaultArgs() {
                let nDefault = 0;
                args.forEach(({type}) => {
                    switch (type) {
                        case 'simpleArgument':
                            if (nDefault !== 0)
                                throw new Error('arguments without default value cannot appear after argument with default value');
                            break;
                        case 'argumentWithDefaultValue':
                            nDefault++;
                            break;
                        default:
                            throw new Error(`unknown type of argument ${type}`);
                    }
                });
                return nDefault;
            }
        },
        objectLiteral: (pairs) => pairs.map(pair => pair.map(analyzeNode)),
        importStatement: ([namespaceAst, depRef]) => {
            return [namespaceAst.map(analyzeNode), analyzeNode(depRef)];
        },
        cond: ([normalCases, elseCase]) => {
            return [
                normalCases.map((parts) => parts.map(analyzeNode)),
                analyzeNode(elseCase)
            ];
        },
        fnCall: ([name, args]) => [analyzeNode(name), args.map(analyzeNode)],
        dimensionCondition: ([name, op, value]) => {
            if (enumConditions[name]) {
                return new Flatten({
                    type: 'enumCondition',
                    value: [name, value]
                });
            }
            if (!dimensions[name]) {
                throw new Error(`unknown media dimension "${name}"`);
            }
            return [name, op, analyzeNode(value)];
        },
        boolCondition: (name) => {
            if (!mediaTypes[name]) {
                throw new Error(`unknown media type "${name}"`);
            }
            return name;
        },
        enumCondition: ([name, value]) => {
            if (!enumConditions[value][name]) {
                throw new Error(`unknown ${name} "${value}"`);
            }
            return [name, value];
        },
        lazyValue: (expr) => {
            return analyzeNode({
                type: 'lambdaExpression',
                value: [
                    [{
                        type: 'simpleArgument',
                        value: {
                            type: 'name',
                            value: 'self'
                        }
                    }],
                    expr
                ]
            });
        },
        keyframesExpression: ([name, rules]) => {
            const nameA = analyzeNode(name);
            const rulesA = rules.map(analyzeNode);
            return [nameA, rulesA];
        },
        keyframesRule: ([selector, assignments]) => {
            return [
                analyzeNode(selector),
                assignments.map(analyzeNode)
            ];
        },
        forExpression: ([[name, rangeStart, rangeEnd], value]) => {
            return new Flatten({
                type: 'fnCall', value: [
                    {type: 'propAccess', value: [
                        {type: 'range', value: [rangeStart, rangeEnd]},
                        {type: 'name', value: 'map'}]},
                    [
                        {type: 'lambdaExpression', value: [
                            [{type: 'simpleArgument', value: name}],
                            value]}]]
            });
        },
        testStatement: ([name, content]) => {
            currentContext.ownContent.names = {};
            return [
                analyzeNode(name),
                content.map(analyzeNode)
            ];
        }
    };

    return astRoot.map(analyzeNode);
};

module.exports = analyzeModule;
