const {Scope, Node} = require('./primitives');

let indent = '';

const format = (chunks) => {
    if (chunks instanceof Node) {
        return chunks.map(format);
    }
    if (chunks instanceof Array) {
        return chunks.map(format);
    }
    if (typeof chunks === 'string') {
        return chunks;
    }
    if (chunks instanceof Scope) {
        switch (chunks.type) {
            case 'ind': {
                const oldInd = indent;
                indent += '  ';
                const res = format(chunks.content);
                indent = oldInd;
                return res;
            }
            case 'lines':
                return chunks.content.map(
                    chunk => ['\n', indent, format(chunk)]
                );
        }
    }
    console.assert(false, 'should never get there');
};

const chunks2code = (rootNode, modules, generateSourceMaps = true) => {
    const chunks2codeR = (chunks) => {
        if (chunks instanceof Node) {
            return chunks2codeR(chunks.content);
        }
        if (chunks instanceof Array) {
            return chunks.map(chunks2codeR).join('');
        }
        if (typeof chunks === 'string') {
            return chunks;
        }
        console.assert(false, 'should never get there');
    };

    const formatted = format(rootNode);
    console.assert(formatted instanceof Node);

    const flattened = formatted.flatten();
    console.assert(flattened instanceof Node);

    if (!generateSourceMaps) {
        return chunks2codeR(flattened);
    }

    const mapped = withSourceMaps(flattened, modules).toStringWithSourceMap();

    const mapsString = mapped.map.toString();
    const mapsBase64 = new Buffer(mapsString).toString('base64');
    const comment = `//# sourceMappingURL=data:application/json;charset=utf-8;base64,${mapsBase64}`;


    return mapped.code + '\n' + comment;
};

const S = require('source-map');

const withSourceMaps = (flattened, modules) => {
    const Root = new S.SourceNode();
    flattened.content.forEach(chunk => {
        if (typeof chunk === 'string') {
            return Root.add(chunk);
        }
        if (chunk instanceof Node) {
            return Root.add(
                chunk.toSourceNode(
                    chunk.data.fileName,
                    modules[chunk.data.fileName].lssCode
                )
            );
        }
        console.assert(false, 'should never get there');
    });

    return Root;
};

module.exports = {chunks2code, format};
