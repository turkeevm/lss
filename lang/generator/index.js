const {ind, lines, Node} = require('./primitives');

const js = (xs, ...ys) => {
    const res = [xs[0]];
    for (let i = 1; i < xs.length; i++) {
        res.push(ys[i - 1]);
        res.push(xs[i]);
    }
    return res;
};

const join = (sep, xs) => {
    if (xs.length === 0) return [];
    const res = [xs[0]];
    for (let i = 1; i < xs.length; i++) {
        res.push(sep);
        res.push(xs[i]);
    }
    return res;
};

const arr = (jss) => js`[${ind(join(', ', jss))}]`;


const iife = xs => js`(${fe(xs)})()`;
const fe = (xs, names = '') => js`(${names}) => {${ind(xs)}}`;

const resolveLocal = x => JSON.stringify(require.resolve(x));

const moduleBody = (ast, fileName) => {
    const mathBinary =
        opName => ([lhs, rhs]) =>
            js`$math.${opName}(${gen(lhs)}, ${gen(rhs)})`;
    const mathUnary =
        opName => x =>
            js`$math.${opName}(${gen(x)})`;

    const names = {};
    const makeName = (base) => {
        const escapedBase = base.replace(/[^a-zA-Z0-9]+/g, '_');
        const n = names[escapedBase] || 0;
        names[escapedBase] = n + 1;

        return n ? `$${escapedBase}_${n - 1}` : `$${escapedBase}`;
    };

    const objLinesOnePropCode = ([keyAst, valueAst]) => {
        switch (keyAst.type) {
            case 'string':
            case 'name': {
                const key = keyAst.value;
                return js`$acc["${key}"] = ${gen(valueAst)};`;
            }
            default:
                throw new Error(`don\'t know how to generate object key from ${keyAst.type}`)
        }
    };
    const objectLiteralLines = (props) => lines([
        'const $acc = {};',
        ...props.map(objLinesOnePropCode),
        'return $acc;'
    ]);


    const gens = {
        emit: exp => js`$acc.css.injectMixin(${gen(exp)});`,
        defStatement: ([name, value]) => js`const ${gen(name)} = ${gen(value)};`,
        defUnitStatement: ([name, value]) =>
            js`$units.register(${JSON.stringify(name.value)}, ${gen(value)});`,
        importStatement: ([namespaceAst, dependencyReferenceAst]) => {
            const dependencyReference = dependencyReferenceAst.value[0].value;
            const namespaceName = namespaceAst.length
                ? gen(namespaceAst[0], true)
                : makeName(dependencyReference);

            return lines([
                js`const ${namespaceName} = $modules[${JSON.stringify(dependencyReference)}]();`,
                js`if (typeof ${namespaceName}.css === "string" && ${namespaceName}.css !== "") {`,
                js`  $acc.css += ${namespaceName}.css + "\\n\\n";`,//fixme: it is not a string anymore
                `}`,
                js`if (typeof ${namespaceName}.testReport === "string" && ${namespaceName}.testReport !== "") {`,
                js`  $acc.testReport += ${namespaceName}.testReport + "\\n";`,
                `}`
            ]);
        },
        exportStatement: (exportedEntity) => {
            switch (exportedEntity.type) {
                case 'defStatement': {
                    const bindingName = gen(exportedEntity.value[0]);
                    return lines([
                        gen(exportedEntity),
                        js`$acc["${bindingName}"] = ${bindingName};`
                    ]);
                }
                default: throw new Error(`don't know how to export ${exportedEntity.type}`);
            }
        },
        consoleStatement: (expr) => js`console.${gen(expr)};`,
        debuggerStatement: () => `debugger;`,
        range: ([lo, hi]) => gens.stepRange([lo, {type: 'unitless', value: 1}, hi]),
        stepRange: ([lo, step, hi]) => [
            `$lists.range(`,
            [gen(lo), ','],
            [gen(hi), ','],
            gen(step),
            `)`
        ],
        media: xs => js`$media.make(${arr(xs.map(gen))})`,
        mediaCase: ([condition, value]) => js`$media.makeCase(${gen(condition)}, ${gen(value)})`,

        dimensionCondition: ([_var, _op, value]) =>
            js`$media.conditions.makeDimmension("${_var}", "${_op}", ${gen(value)})`,
        blockExpression: ([context, value]) => {
            if (context.length === 0) {
                return gen(value);
            }
            return iife(gens.blockExpressionContent([context, value]));
        },
        blockExpressionContent: ([context, value]) => lines([
            ...context.map(gen),
            js`return (${gen(value)});`
        ]),
        interpolation: x => gen(x),
        cssExpression: x => gen(x),
        cssRuleSet: x => iife(lines([
            `const $builder = $css.builder();`,
            gen(x),
            `return $builder.frame;`
        ])),
        cssRuleSetContent: xs => lines(xs.map(gen)),
        cssRule: ([selector, children], {start, source}) => lines([
            js`$builder.enterSelector(${gen(selector)}, ${JSON.stringify({start, source: fileName})});`,
            ind(gen(children)),
            '$builder.exitSelector();'
        ]),
        cssSelector: chunks => gens.interpolatedString(chunks),
        cssSelectorChunk: _c => JSON.stringify(_c),
        cssPropAssignment: ([name, value]) => js`$builder.prop(${gen(name)}, ${gen(value)});`,
        cssPropName: chunks => gens.interpolatedString(chunks),
        cssPropNameChunk: _c => JSON.stringify(_c),
        cssName: chunks => js`$css.makeCssName(${gens.interpolatedString(chunks)})`,
        cssNameChunk: _c => JSON.stringify(_c),
        cssString: chunks => js`$css.makeCssString(${gens.interpolatedString(chunks)})`,
        cssFraction: ([a, b]) => js`$css.makeFraction(${gen(a)}, ${gen(b)})`,
        cssSpaceList: xs => js`$lists.spaceList(${join(', ', xs.map(gen))})`,
        cssCommaList: xs => js`$lists.commaList(${join(', ', xs.map(gen))})`,
        mixinInjection: expr => js`$builder.injectMixin(${gen(expr)})`,
        lazyValue: expr => js`(${gen(expr)})($builder.frame)`,
        keyframesExpression: ([name, rules]) => iife(lines([
            js`const $builder = $css.keyframesBuilder(${JSON.stringify(name)});`,
            ...rules.map(x => ind(gen(x))),
            'return $builder.frame;'
        ])),
        keyframesRule: ([selector, children]) => lines([
            js`$builder.enterKeyframe(${gen(selector)});`,
            ...children.map(x => ind(gen(x))),
            '$builder.exitKeyframe();'
        ]),
        name: _n => _n,
        withUnit: ([_val, _unit]) => js`$units.resolve(${_val.toString()}, ${JSON.stringify(_unit)})`,
        colorHex: _hex => js`$css.makeColor("#${_hex}")`,
        nullVal: () => 'null',
        cssFunction: ([name, args]) => js`$css.makeCssFunction(${gen(name)}, ${gen(args)})`,
        interpolatedString: (chunks) => {
            if (chunks.length === 0) {
                return JSON.stringify('');
            }
            if (chunks.length === 1) {
                return gen(chunks[0]);
            }
            if (chunks.length === 2) {
                return js`$math.add(${gen(chunks[0])}, ${gen(chunks[1])})`;
            }
            return js`${arr(chunks.map(gen))}.reduce($math.add)`;
        },
        stringChunk: _c => JSON.stringify(_c),
        unitless: _u => _u.toString(),
        boolValue: x => x ? 'true' : 'false',
        lambdaExpression: ([nDefault, args, body]) => {
            const namesCode = join(', ', args.map((x, i) => {
                const name = gen(
                    x.type === 'simpleArgument'
                        ? x.value
                        : x.value[0]
                );
                if (i < nDefault) {
                    const value = gen(x.value[1]);
                    return js`${name} = ${value}`;
                }
                return name;
            }));

            return fe(gen(body), namesCode);
        },
        simpleReturn: (value) => js`return (${gen(value, true)})`,
        fnCall: ([name, args]) => {
            //todo: test. media function, media context
            if (name.type === 'propAccess' || name.type === 'computedPropAccess') {
                const {type: accessType, value: [context, fnName]} = name;
                return gen({
                    type: 'contextCall',
                    value: [accessType, context, fnName, args]
                });
            }
            return js`${gen(name, true)}(${join(', ', args.map(gen))})`
        },
        contextCall: ([accessType, context, fnName, args]) => {
            let accessCode = '';
            if (accessType === 'propAccess') {
                accessCode = js`.${gen(fnName, true)}`;
            } else if (accessType === 'computedPropAccess') {
                accessCode = js`[${gen(fnName, true)}]`;
            } else {
                throw new Error(`unknown property access type ${accessType}`);
            }

            return (
                js`(${gen(context)})${accessCode}(${join(', ', args.map(gen))})`
            );
        },
        objectLiteral: props => iife(objectLiteralLines(props)),
        propAccess: ([obj, key]) => js`$math.access(${gen(obj)}, ${JSON.stringify(key.value)})`,
        computedPropAccess: ([obj, key]) => js`$math.access(${gen(obj)}, ${gen(key)})`,
        roundBlock: (x) => js`(${gen(x)})`,
        cond: ([normalCases, elseCase]) => {
            /**
             * $aware.awareApply(($cond, $then) =>
             *   $cond()
             *     ? $then()
             *     : <else>,
             *   [
             *     () => cond,
             *     () => then
             *   ]
             * )
             */

            return normalCases
                .slice()
                .reverse()
                .reduce((elseCode, [predExpr, valueExpr]) => [
                    `$aware.awareApply(($cond, $then) => {`,
                        ind(js`return $cond ? $then() : (${elseCode});`),
                    '}, [',
                        ind(gen(predExpr)), ',',
                        ind(fe(js`return (${gen(valueExpr)});`)),
                    `])`
                ], gen(elseCase, true));
        },
        gt: mathBinary('gt'),
        lt: mathBinary('lt'),
        gte: mathBinary('gte'),
        lte: mathBinary('lte'),
        eq: mathBinary('eq'),
        neq: mathBinary('neq'),

        sub: mathBinary('sub'),
        add: mathBinary('add'),
        addPoly: mathBinary('addPoly'),
        mul: mathBinary('mul'),
        div: mathBinary('div'),

        and: mathBinary('and'),
        or: mathBinary('or'),
        not: mathUnary('not'),


        testStatement: ([name, xs]) => js`$test.test(${gen(name)}, () => {${ind(lines(xs.map(gen)))}}, $accTestReports);`,
        assertStatement: ([type, name, ...args]) =>
            js`$test.assert(${gen(type)}, ${gen(name)}, ${arr(args.map(x => ind(lines([js`() => ${gen(x)}`]))))});`
    };

    const gen = ast => {
        const type = ast.type;
        const generator = gens[type];
        if (!generator) {
            throw new Error(`no such generator ${type}`);
        }

        const chunks = generator(ast.value, ast);

        //format(chunks);

        return new Node(chunks, ast);
    };

    return new Node(iife(ind(lines([
        `const $css = require(${resolveLocal('../../runtime/css')});`,
        `const $units = require(${resolveLocal('../../runtime/units')});`,
        `const $lists = require(${resolveLocal('../../runtime/lists')});`,
        `const $math = require(${resolveLocal('../../runtime/math')});`,
        `const $media = require(${resolveLocal('../../runtime/media')});`,
        `const $aware = require(${resolveLocal('../../runtime/aware')});`,
        `const $stdlib = require(${resolveLocal('../../runtime/stdlib')});`,
        `const __FILE = ${JSON.stringify(fileName)};`,
        `const $fileTestRoot = $test.makeFileTestRoot(__FILE);`,
        `$globalTestRoot.children.push($fileTestRoot);`,
        ...Object.keys(require('../../runtime/stdlib'))
            .map(name => `const ${name} = $stdlib.${name};`),
        `const $acc = {css: $css.builder(), testReport: $globalTestRoot};`,
        `const $accTestReports = ($frame) => $fileTestRoot.children.push($frame);`,
        ``,
        ...ast.map(gen),
        `return $acc;`
    ]))), ast);
};

const generateModules = (entryPoint, modules) => {

    const oneModule = (moduleAst, fileName) => new Node(iife(lines([
        'let isCalled = false;',
        'let cachedResult = null;',
        ['return ', fe(lines([
            `if (!isCalled) {`,
            ind(js`cachedResult = ${moduleBody(moduleAst, fileName)};`),
            ind(['isCalled = true;']),
            '}',
            'return cachedResult;'
        ]), '')]
    ])), {fileName});


    return new Node(iife(lines([
        `const $modules = {};`,
        `const $test = require(${resolveLocal('../../runtime/test')});`,
        `const $globalTestRoot = $test.makeGlobalTestRoot();`,
        ...Object.keys(modules).map((fileName) => {
            const module = modules[fileName];
            return js`$modules[${JSON.stringify(fileName)}] = ${oneModule(module.ast, fileName)};`;
        }),
        `return $modules[${JSON.stringify(entryPoint)}]();`
    ])));
};

module.exports = generateModules;
