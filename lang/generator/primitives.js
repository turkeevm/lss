const S = require('source-map');
const _ = require('lodash');

class Scope {
    constructor(type, content) {
        this.type = type;
        this.content = content;
    }
}

const flatten = (nodeContent) => {
    const res = [];
    const flattenR = (content) => {
        for (let i = 0; i < content.length; i++) {
            const item = content[i];
            if (typeof item === 'string') {
                if (typeof res[res.length - 1] === 'string') {
                    res[res.length - 1] += item;
                } else {
                    res.push(item)
                }
            } else if (item instanceof Node) {
                res.push(item.flatten());
            } else if (item instanceof Array) {
                flattenR(item);
            } else {
                console.assert(false, 'should never get there');
            }
        }
    };
    flattenR(nodeContent);

    return res;
};

const indexes = new WeakMap();

const ensureIndexFor = (sourceCode) => {
    const re = /\n(\r?)/g;
    if (!indexes.has(sourceCode)) {
        const index = [0];
        for (let i = 0; i < sourceCode.length;) {
            const res = re.exec(sourceCode);
            if (!res) break;
            index.push(res.index + res[0].length);
        }
        indexes.set(sourceCode, index);
    }
    return indexes.get(sourceCode);
};

const locationFor = (offset, sourceCode) => {
    const index = ensureIndexFor(sourceCode);
    const line = _.sortedLastIndex(index, offset);
    const column = offset - index[line - 1] + 1;
    return {
        line,
        column
    };
};

class Node {
    constructor(content, data) {
        this.content = content;
        this.data = data;
    }

    flatten() {
        if (typeof this.content === 'string') {
            return this;
        }
        if (this.content instanceof Array) {
            return new Node(flatten(this.content), this.data);
        }
        if (this.content instanceof Node) {
            return this.content.flatten();
        }
        console.assert(false, 'should never get there');
    }

    map(f) {
        return new Node(f(this.content), this.data);
    }

    toSourceNode(fileName, sourceCode) {
        if ('start' in this.data) {
            return this.toMappedSourceNode(fileName, sourceCode);
        }
        return this.toPlainSourceNode(fileName, sourceCode);
    }

    toMappedSourceNode(fileName, sourceCode) {
        const {line, column} = locationFor(this.data.start, sourceCode);
        if (typeof this.content === 'string') {
            return new S.SourceNode(line, column, fileName, this.content);
        }
        if (this.content instanceof Array) {
            return new S.SourceNode(line, column, fileName, this.content.map(chunk => {
                if (typeof chunk === 'string') return chunk;
                if (chunk instanceof Node) return chunk.toSourceNode(fileName, sourceCode);
            }));
        }
        console.assert(false, 'should never get there');
    }
    toPlainSourceNode(fileName, sourceCode) {
        const N = new S.SourceNode();
        if (typeof this.content === 'string') {
            N.add(this.content);
            return N;
        }
        if (this.content instanceof Array) {
            this.content.forEach(chunk => {
                if (typeof chunk === 'string') {
                    return N.add(chunk);
                }
                if (chunk instanceof Node) {
                    return N.add(chunk.toSourceNode(fileName, sourceCode));
                }
                console.assert(false, 'should never get there');
            });

            return N;
        }
        console.assert(false, 'should never get there');
    }
}

const ind = (x) => new Scope('ind', x);
const lines = (x) => new Scope('lines', x);

module.exports = {Scope, Node, ind, lines, locationFor};
