const {fail, regexp, succeed, seqMap, lazy, seq, string, alt} = require('parsimmon');
const {optWhitespace} = require('./lex');
const {roundBlock, spaced, withValidation} = require('./util');

function operators(ops) {
  const keys = Object.keys(ops).sort();
  const ps = keys.map((k) => spaced(string(ops[k])).result(k));
  return alt(...ps);
}

function PREFIX(operatorsParser, nextParser, validate) {
  if (validate) {
    throw new Error('not implemented yet');
  }
  const parser = lazy(() =>
      seq(operatorsParser, parser)
          .map(([type, value]) => ({type, value}))
          .or(nextParser)
  );
  return parser;
}

function POSTFIX(operatorsParser, nextParser, validate) {
  if (validate) {
    throw new Error('not implemented yet');
  }
  return seqMap(
    nextParser,
    operatorsParser.many(),
    (initialValue, types) => types.reduce((value, type) => ({type, value}), initialValue)
  );
}

function BINARY_RIGHT(operatorsParser, nextParser, validate) {
  if (validate) {
    throw new Error('not implemented yet');
  }
  const parser = lazy(() =>
    nextParser.chain((next) =>
        seq(
            operatorsParser,
            succeed(next),
            parser
        )
        .or(
            succeed(next)
        )
    )
  );
  return parser;
}

function BINARY_LEFT(operatorsParser, nextParser, validatorDescriptor) {
  const chainLink = validatorDescriptor
      ? withValidation(
          seq(operatorsParser, nextParser),
          ({type, value: [rhs]}) => validatorDescriptor[1](rhs, type),
          validatorDescriptor[0]
      )
      : seq(operatorsParser, nextParser);
  return seqMap(
    nextParser,
    chainLink.many(),
    (first, rest) =>
        rest.reduce((acc, ch) => {
            const op = ch[0];
            const another = ch[1];
            return {type: op, value: [acc, another]};
        }, first)
  );
}

const makeMath = (leafParser, table) => {
    const Basic = lazy(() => 
      alt(
        leafParser,
        roundBlock(math).annotate('roundBlock')
      )
    );

    const math = spaced(table.reduce((acc, level) => level.type(level.ops, acc, level.validate), Basic));

    return math;
};

module.exports = {
    makeMath,
    PREFIX,
    POSTFIX,
    BINARY_LEFT,
    BINARY_RIGHT,
    operators
};
