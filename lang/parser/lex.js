const Parsimmon = require('parsimmon');
const {fail, succeed, lazy, string, regex, seqMap, alt, sepBy, sepBy1} = Parsimmon;
const {withCache} = require('./cache');
const {withValidation, logger} = require('./util');

const optWhitespace = regex(/(\s|(\/\/[^\n]*\n)|(\/\*(.|[\r\n])*?\*\/))*/);
const whitespace = regex(/(\s|(\/\/[^\n]*\n)|(\/\*(.|[\r\n])*?\*\/))+/).desc('whitespaceOrComment');

const JS_KEY_WORDS = 'instanceof typeof break do new var case else return void catch finally continue for switch while this with debugger function throw default if try delete in'.split(' ');
const RESERVED = 'keyframes media true false null export def else emit css cond'.split(' ');

const reservedIdx = JS_KEY_WORDS.concat(RESERVED).reduce((acc, key) => {
    acc[key] = true;
    return acc;
}, Object.create(null));

const CURLY_OP = string('{');
const CURLY_CL = string('}');
const ROUND_OP = string('(');
const ROUND_CL = string(')');
const SQUARE_OP = string('[');
const SQUARE_CL = string(']');
const COLON = string(':');
const SEMICOLON = string(';');
const COMMA = string(',');
const BACKTICK = string('`');
const HASH = string('#');
const ASSIGN = string('=');
const SINGLE_QUOTE = string('\'');
const FAT_ARROW = string('=>');
const PLUS = string('+');
const DOT = string('.');

const iNumber = regex(/-?\d+/);
const fNumber = regex(/-?\d*\.\d+/);
const eNumber = regex(/-?\d?(\.\d+)?e(-)?\d+/);

const unit = regex(/[a-z%]+/);

const number = withCache(alt(
    eNumber,
    fNumber,
    iNumber
).map(x => +x).desc('number'));

const boolVal = string('false').or(string('true')).map(x => x === 'true').annotate('boolValue');
const nullVal = string('null').annotate('nullVal');

const name = lazy(() => withValidation(
    regex(/[a-zA-Z_\xA0-\uFFFF][a-zA-Z_0-9\xA0-\uFFFF]*/),
    (name) => !(name in reservedIdx),
    'variable name'
)).annotate('name');

module.exports = {
    whitespace,
    optWhitespace,
    name,
    number,
    unit,
    boolVal,
    nullVal,
    CURLY_OP,
    CURLY_CL,
    ROUND_OP,
    ROUND_CL,
    SQUARE_OP,
    SQUARE_CL,
    COLON,
    SEMICOLON,
    COMMA,
    BACKTICK,
    ASSIGN,
    HASH,
    FAT_ARROW,
    PLUS,
    DOT,
    SINGLE_QUOTE
};
