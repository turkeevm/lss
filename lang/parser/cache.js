const Parsimmon = require('parsimmon');

let counter = 0;
let invalidators = [];

function withCachingEnabled(parser) {
    return Parsimmon((input, i) => {
        counter++;
        let result = parser._(input, i);
        counter--;

        if (counter === 0) {
            invalidators.forEach(invalidator => {
                invalidator();
            });
            invalidators = [];
        }
        return result;
    })
}

function withCache(parser) {
    let cache = {};
    let isRegistered = false;
    return Parsimmon((input, i) => {
        if (!counter) {
            return parser._(input, i);
        }
        if (!isRegistered) {
            invalidators.push(() => {
                cache = {};
                isRegistered = false;
            });
            isRegistered = true;
        }
        if (!cache[i]) {
            cache[i] = parser._(input, i);
        }

        return cache[i];
    });
}

Parsimmon.prototype.withCache = function () {
    return withCache(this);
};

Parsimmon.prototype.withCachingEnabled = function () {
    return withCachingEnabled(this);
};

module.exports = {withCache, withCachingEnabled};
