const {roundBlock, curlyBlock, spaced, main, seqAnnotate} = require('./util');
const {lazy, string, regex, seqMap, alt, sepBy} = require('parsimmon');
const {whitespace, optWhitespace, HASH, COLON, SEMICOLON, BACKTICK, number, unit, name, COMMA, PLUS, NAME_START} = require('./lex');
const {withCache} = require('./cache');

const expression = main(({expression}) => expression);
const interpolation = main(({interpolation}) => interpolation);
const inlineExpression = main(({inlineExpression}) => inlineExpression);
const interpolatedString = main(({interpolatedString}) => interpolatedString);

const propertyName = alt(
    regex(/[a-zA-Z\-]+/).annotate('cssPropNameChunk'),
    interpolation
)
.atLeast(1)
.annotate('cssPropName');

const cssFunction = lazy(() => seqAnnotate('cssFunction', cssName, roundBlock(cssCommaList)));

const cssNameChunk = regex(/[a-zA-Z\-][a-zA-Z\-0-9]*/)
    .annotate('cssNameChunk')
    .desc('css word');

const cssName = alt(
    cssNameChunk,
    interpolation
)
.atLeast(1)
.map(chunks => {
    if (chunks.length === 1 && chunks[0].type === 'interpolation') {
        return chunks[0];
    }
    return {type: 'cssName', value: chunks};
})
.debug('cssName');

const color = HASH.then(
    alt(
        regex(/[0-9a-fA-F]{6}/),
        regex(/[0-9a-fA-F]{3}/)
    ))
    .annotate('colorHex')
    .desc('color literal');

const lengthOrNumber = seqAnnotate(
    'withUnit',
    number,
    unit.atMost(1)
).map((node) => {
    if (node.value[1].length === 0) {
        return {
            ...node,
            type: 'unitless',
            value: node.value[0]
        };
    } else {
        return {
            ...node,
            type: 'withUnit',
            value: [
                node.value[0],
                node.value[1][0]//because maybe
            ]
        };
    }
});

const lazyValue = string('^')
    .then(inlineExpression)
    .annotate('lazyValue');


const cssSpaceListItem = alt(
    lazyValue,
    cssFunction,
    cssName,
    color,
    lengthOrNumber,
    interpolatedString.map(x => x.value).annotate('cssString')
);

const cssSpaceList = sepBy(cssSpaceListItem, whitespace)
    .validate(items => items.length >= 1)
    .map((items) => items.length === 1 ? items[0] : {type: 'cssSpaceList', value: items})
    .debug('cssSpaceList');

const cssCommaListItem = seqMap(
    cssSpaceList,
    spaced(string('/')).then(cssSpaceList).atMost(1),
    (first, maybeSecond) => maybeSecond.length === 1
        ? {type: 'cssFraction', value: [first, maybeSecond[0]]}
        : first
);

const cssCommaList = sepBy(cssCommaListItem, spaced(string(',')))
    .validate(items => items.length >= 1)
    .map((items) => items.length === 1 ? items[0] : {type: 'cssCommaList', value: items})
    .debug('cssCommaList');

const propertyValue = cssCommaList;

const cssSelectorChunk = regex(/(([&a-zA-Z\d#\.=_*\-^~\+>,\(\)\[\]]\s*)|:+([&a-zA-Z#\.=_*\-^~\+>,\(\)\[\]]))+[&a-zA-Z\d#\.=_*\-^~\+>:,\(\)\[\]]/)
    .desc('cssSelectorChunk')
    .annotate('cssSelectorChunk');

const cssSelector = alt(cssSelectorChunk, interpolation)
    .atLeast(1)
    .annotate('cssSelector');

const rule = lazy(() => seqAnnotate(
    'cssRule',
    cssSelector.skip(optWhitespace),
    curlyBlock(cssTree)
));

const assignment = seqAnnotate(
    'cssPropAssignment',
    propertyName.skip(spaced(string(':'))),
    propertyValue.skip(optWhitespace).skip(string(';'))
);

const mixinExpression = string('+')
    .then(expression.spaced())
    .skip(string(';'))
    .annotate('mixinInjection');

const cssTree = 
    sepBy(
        alt(
            rule,
            assignment,
            mixinExpression
        ).debug('cssRuleSetContent.item'),
        optWhitespace.debug('cssRuleSetContent.spaces')
    )
    .annotate('cssRuleSetContent');

const cssExpressionContent = alt(
    roundBlock(cssCommaList).validate(x => x.type !== 'interpolation'),
    cssFunction,
    cssName,
    curlyBlock(cssTree).annotate('cssRuleSet')
);

const cssExpression = 
    alt(
        string('css').skip(optWhitespace),
        string('`')
    )
    .then(cssExpressionContent)
    .annotate('cssExpression');

const keyframesExpression = string('keyframes')
    .skip(whitespace)
    .then(seqAnnotate(
        'keyframesExpression',
        regex(/[a-zA-Z0-9-]+/).skip(whitespace),
        curlyBlock(sepBy(
            seqAnnotate(
                'keyframesRule',
                inlineExpression.skip(optWhitespace),
                curlyBlock(sepBy(assignment, optWhitespace))
            ),
            optWhitespace
        ))
    ));

module.exports = {keyframesExpression, cssNameChunk, cssExpression, lengthOrNumber, color};
