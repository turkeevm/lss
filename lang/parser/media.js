const {fail, succeed, lazy, string, regex, seqMap, seq, alt, sepBy, sepBy1} = require('parsimmon');
const {roundBlock, curlyBlock, logger, main, spaced, seqAnnotate, css} = require('./util');
const {optWhitespace, COLON, SEMICOLON} = require('./lex');

const expression = main(({expression}) => expression);

const mediaTypeCondition = regex(/[a-z]+/).annotate('boolCondition');

const dimensionCondition = seqAnnotate(
    'dimensionCondition',
    regex(/[a-z-]+/).skip(optWhitespace),
    regex(/([<>]=?)|==/).skip(optWhitespace),
    alt(
        css(({lengthOrNumber, cssNameChunk}) => alt(lengthOrNumber, cssNameChunk)),
        main(({interpolation}) => interpolation)
    )
);

const singleCondition = alt(
    dimensionCondition,
    mediaTypeCondition
);

const mediaCondition = alt(
    roundBlock(expression),
    singleCondition
);

const mediaCase = seqAnnotate(
    'mediaCase',
    mediaCondition.skip(spaced(COLON)),
    expression
);

const mediaExpression = string('media')
    .skip(optWhitespace)
    .then(curlyBlock(
        mediaCase
            .skip(spaced(SEMICOLON))
            .many()
    ))
    .annotate('media');

module.exports = mediaExpression;
