const {program} = require('./main');
module.exports = (s) => program.tryParse(s);
