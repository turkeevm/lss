const {succeed, lazy, string, regex, seq, seqMap, alt, sepBy} = require('parsimmon');
const {operators, makeMath, PREFIX, BINARY_LEFT} = require('./math');
const {squareBlock, roundBlock, curlyBlock, spaced, css, count, seqAnnotate} = require('./util');
const {whitespace, optWhitespace, name, boolVal, nullVal, COMMA, COLON, SEMICOLON, FAT_ARROW, ASSIGN, DOT, SINGLE_QUOTE} = require('./lex');
const mediaExpression = require('./media');

const inlineExpression = lazy(() =>
    alt(
        standaloneExpression,
        roundBlock(expression)
    )
).debug('inlineExpression');

const interpolation = string('$')
    .then(inlineExpression)
    .annotate('interpolation');

const stringChunk = regex(/([^\n'$\\]|(\\([\\$'nrt])))+/)
    .map(x => x.replace(/\\([\\$'nrt])/g, m => JSON.parse(`"${m}"`)))
    .annotate('stringChunk');

const interpolatedString = SINGLE_QUOTE.then(
    alt(
        stringChunk,
        interpolation
    ).many()
).skip(SINGLE_QUOTE).annotate('interpolatedString');

const tail = first => alt(
    spaced(DOT).then(name).annotate('propAccessKey'),
    roundBlock(sepBy(expression, spaced(COMMA))).annotate('fnCallArgs'),
    squareBlock(expression).annotate('computedPropAccessKey')
    )
    .many()
    .map(rest =>
        rest.reduce((lhs, {type, value: rhs}) => {
            const value = [lhs, rhs];
            switch (type) {
                case 'propAccessKey':
                    return {type: 'propAccess', value};
                case 'fnCallArgs':
                    return {type: 'fnCall', value};
                case 'computedPropAccessKey':
                    return {type: 'computedPropAccess', value};
                default:
                    throw new Error('something went wrong');
            }

        }, first)
    );

const forExpression = string('for')
    .skip(optWhitespace)
    .then(seqAnnotate(
        'forExpression',
        roundBlock(
            seq(
                name.skip(spaced(ASSIGN)),
                inlineExpression.skip(spaced(string('..'))),
                inlineExpression
            )
        ),
        lazy(() => expression)
    ));

const standaloneExpression = css(({cssExpression, lengthOrNumber, color, keyframesExpression}) => alt(
    lengthOrNumber,
    lambdaExpression,
    name,
    cssExpression,
    color,
    forExpression,
    blockExpression,
    condExpression,
    objectLiteral,
    mediaExpression,
    keyframesExpression,
    range,
    interpolatedString,
    boolVal,
    nullVal
)).chain(tail).withCache();

const lambdaArguments = lazy(() => alt(
    name
        .annotate('simpleArgument')
        .map(x => [x]),
    roundBlock(
        sepBy(
            alt(
                seqAnnotate(
                    'argumentWithDefaultValue',
                    name.skip(spaced(ASSIGN)),
                    expression
                ),
                name.annotate('simpleArgument')
            ),
            spaced(COMMA)
        )
    )
));

const lambdaExpression = seqAnnotate(
    'lambdaExpression',
    lambdaArguments.skip(spaced(FAT_ARROW)),
    lazy(() => expression)
);

// cond
const condEntry = lazy(() =>
    seq(
        expression.skip(spaced(COLON)),
        expression.skip(spaced(SEMICOLON))
    )
);

const elseEntry = lazy(() =>
    string('else')
        .skip(spaced(COLON))
        .then(expression)
        .skip(SEMICOLON)
);

const condExpression =
    string('cond')
        .skip(optWhitespace)
        .then(curlyBlock(
            seqAnnotate(
                'cond',
                sepBy(condEntry, optWhitespace).skip(optWhitespace),
                elseEntry
            )
        ));

// literal
const keyLiteral = css(({lengthOrNumber, color}) => alt(
    lengthOrNumber,
    color,
    interpolatedString,
    name
));

// objectLiteral
const objectLiteral = curlyBlock(
    sepBy(
        seq(
            keyLiteral.skip(spaced(COLON)),
            lazy(() => expression)
        ),
        spaced(COMMA)
    )
)
.annotate('objectLiteral');

//range
const range = lazy(() => squareBlock(seqMap(
    expression.skip(spaced(COLON)),
    expression,
    alt(
        spaced(COLON).then(expression),
        string('')
    ),
    (a, b, c) => c
        ? {type: 'stepRange', value: [a, b, c]}
        : {type: 'range', value: [a, b]}
))).debug('range');

const assertStatement = string('assert')
    .then(roundBlock(
        sepBy(lazy(() => expression), spaced(COMMA))
    ))
    .skip(SEMICOLON)
    .annotate('assertStatement');

const testStatement = string('test').skip(whitespace).then(
    seqAnnotate(
        'testStatement',
        interpolatedString.skip(whitespace),
        curlyBlock(
            sepBy(
                lazy(() => alt(
                    assertStatement,
                    testStatement,
                    defStatement.skip(spaced(SEMICOLON)),
                    debuggerStatement.skip(spaced(SEMICOLON))
                )),
                optWhitespace
            )
        )
    )
);
const consoleStatement = lazy(() => string('console.').then(expression).annotate('consoleStatement'));

//block expression
const blockExpression = lazy(() =>
    curlyBlock(
        seqAnnotate(
            'blockExpression',
            alt(
                consoleStatement,
                defStatement
            ).skip(spaced(SEMICOLON)).many(),
            expression.skip(spaced(SEMICOLON))
        )
    )
);

const expression = css(() => makeMath(
    standaloneExpression,
    [
        {type: BINARY_LEFT, ops: operators({lt: '<', gt: '>', lte: '<=', gte: '>=', eq: '==', neq: '!='})},
        {type: PREFIX, ops: operators({negate: '-'})},
        {type: BINARY_LEFT, ops: operators({mul: '*', div: '/'})},
        {type: BINARY_LEFT, ops: operators( {add: '+', sub: '-'})},
        {type: BINARY_LEFT, ops: operators({addPoly: '++', subPoly: '--'})},
        {type: PREFIX, ops: operators({not: '!'})},
        {type: BINARY_LEFT, ops: operators({and: '&&', or: '||'})},
    ]
)).chain(lhs => {
    if (lhs.type === 'roundBlock') {
        return tail(lhs);
    }
    return succeed(lhs);
});

const emitStatement =
    string('emit')
        .then(optWhitespace)
        .then(expression)
        .annotate('emit');

const defStatement = seqAnnotate(
    'defStatement',
    string('def').skip(whitespace).then(name).skip(spaced(ASSIGN)),
    expression
);

const defUnitStatement = seqAnnotate(
    'defUnitStatement',
    string('defunit').skip(whitespace).then(name).skip(spaced(ASSIGN)),
    expression
);

const importStatement = string('import').skip(whitespace).then(
    seqAnnotate(
        'importStatement',
        name.skip(spaced(string('from'))).atMost(1),
        interpolatedString
            .validate(({type, value: xs}) =>
                xs.length === 1 && xs[0].type === 'stringChunk',
                'dependency reference')));

const exportStatement = string('export').skip(whitespace).then(defStatement).annotate('exportStatement');

const debuggerStatement = string('debugger').annotate('debuggerStatement');

const statement = alt(
    testStatement,
    defUnitStatement,
    defStatement,
    emitStatement,
    exportStatement,
    importStatement,
    consoleStatement,
    debuggerStatement
).skip(optWhitespace).skip(SEMICOLON);

const program = spaced(sepBy(statement, optWhitespace));

module.exports = {
    program,
    expression,
    interpolation,
    inlineExpression,
    interpolatedString
};
